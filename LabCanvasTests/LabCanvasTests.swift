//
//  LabCanvasTests.swift
//  LabCanvasTests
//
//  Created by Victor Salazar on 6/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import XCTest
@testable import LabCanvas

class LabCanvasTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAssets() throws {
        testTabBar()
        testIcons()
        testImages()
        testBackgrounds()
        testComponents()
    }

    func testTabBar() {
        test(image: .icHome)
        test(image: .icBenefits)
        test(image: .icStore)
        test(image: .icShoppingCart)
        test(image: .icMore)
    }

    func testIcons() {
        test(image: .icMedal)
        test(image: .icBell)
        test(image: .icBulb)
        test(image: .icBag)
        test(image: .icLabcard)
        test(image: .icUser)
        test(image: .icLocation)
        test(image: .icKey)
        test(image: .icGetOut)
        test(image: .icFB)
        test(image: .icGoogle)
        test(image: .icPromotions)
        test(image: .icSearch)
        test(image: .icArrowDown)
        test(image: .icFilter)
        test(image: .icArm)
        test(image: .icLighting)
        test(image: .icBack)
        test(image: .icCalendar)
        test(image: .icShare)
        test(image: .icWaist)
        test(image: .icChronometer)
        test(image: .icChest)
        test(image: .icGymWeight)
        test(image: .icClose)
    }

    func testImages() {
        test(image: .imgSwitch)
        test(image: .imgLab)
        test(image: .imgHome)
        test(image: .imgVerticalPromotions)
    }

    func testBackgrounds() {
        test(image: .bgGirl)
        test(image: .bgBoy)
    }

    func testComponents() {
        test(image: .icChecked)
        test(image: .icUnchecked)
        test(image: .icCircleChecked)
        test(image: .icCircleUnchecked)
        test(image: .icSwitch)
    }

    private func test(image: UIImage) {
        XCTAssert(image.pngData() != nil)
    }

}
