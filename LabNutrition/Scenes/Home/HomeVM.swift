//
//  HomeVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 29/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import LabFoundation

protocol HomeVMProtocol: BaseVMProtocol {

    var displayGreetings: Observable<String> { get set }
    var displayNews: Observable<[Any]> { get set }
    var displayPromotion: Observable<Any> { get set }

}

class HomeVM: HomeVMProtocol {

    private let user = Session.shared.user!
    private var promotionIsLoaded = false
    private var newsIsLoaded = false

    var displayGreetings = Observable<String>()
    var displayNews = Observable<[Any]>()
    var displayPromotion = Observable<Any>()

    func loadView() {
        displayGreetings.notify(with: "Hola \(user.fullName),")
        if !promotionIsLoaded {
            loadPromotion()
        }
        if !newsIsLoaded {
            loadNews()
        }
        loadTest()
    }

    private func loadNews() {
        newsIsLoaded = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.displayNews.notify(with: [0, 1, 2, 3, 4])
        }
    }

    private func loadPromotion() {
        promotionIsLoaded = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.displayPromotion.notify(with: 1)
        }
    }

    private func loadTest() {
        CoreRequestManager.shared.load(ProductListRequest()) { (result) in
            switch result {
            case .success(let response):
                print("response: success: \(response.count)")
            case .failure(let error):
                print("error: \(error)")
            }
        }
    }

}
