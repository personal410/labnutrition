//
//  HomeCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class HomeCoordinator: BaseCoordinator {

    override func start() {
        let viewController: HomeVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = HomeVM()
        push(viewController: viewController)
    }

    func showPromotionsFlow() {
        let coordinator = PromotionsCoordinator(presenter: presenter)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
    }

    func showBlog() {
        let coordinator = BlogCoordinator(presenter: presenter)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
    }

    func showBenefits() {
        guard let mainCoordinator = parent as? MainCoordinator else { return }
        mainCoordinator.changeTab(with: .benefits)
    }

}
