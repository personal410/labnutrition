//
//  HomeVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class HomeVC: BaseVC {

    // MARK: - Props
    weak var coordinator: HomeCoordinator!
    var viewModel: HomeVMProtocol!
    var arrNews: [Any] = []

    // MARK: - Outlet
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var ivWelcome: UIImageView!
    @IBOutlet weak var lblGreetings: UILabel!
    @IBOutlet weak var lblWelcomeApp: UILabel!
    @IBOutlet weak var lblBenefits: UILabel!
    @IBOutlet weak var ivBenefitsArrow: UIImageView!
    @IBOutlet weak var cvNews: UICollectionView!
    @IBOutlet weak var lcSpacingPromotionsNews: NSLayoutConstraint!
    @IBOutlet weak var vPromotions: UIView!
    @IBOutlet weak var vVerticalPromotions: UIView!
    @IBOutlet weak var ivVerticalPromotions: UIImageView!
    @IBOutlet weak var ivPromotions: UIImageView!
    @IBOutlet weak var lblPromotionProduct: UILabel!
    @IBOutlet weak var lblPreviousPrice: UILabel!
    @IBOutlet weak var lblPromotionPrice: UILabel!
    @IBOutlet weak var vLoadingNews: UIView!
    @IBOutlet weak var aiNews: UIActivityIndicatorView!

    // MARK: - Views
    let btnPromotions = UIButton.create(with: .promotion)
    let btnSearch = UIButton.create(with: .search)

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Setup
    override func setupView() {
        navigationView.navigationStyle = .red
        navigationView.useImage = true
        navigationView.backButtonIsHidden = true
        navigationView.addTrailing([btnPromotions, btnSearch])
        btnPromotions.addTarget(self, action: #selector(actionPromotions), for: .touchUpInside)
        ivWelcome.image = .imgHome
        // MARK: - Label Greetings
        lblGreetings.font = .labFont(with: 25)
        lblGreetings.textColor = .white
        lblGreetings.numberOfLines = 2
        // MARK: - Label Welcome App
        lblWelcomeApp.font = .labFont(with: 18)
        lblWelcomeApp.textColor = .projectYellow
        lblWelcomeApp.text = "Bienvenido(a) a tu APP"
        // MARK: - Label Benefits
        lblBenefits.font = .labFont(with: 13, and: .bold)
        lblBenefits.textColor = .white
        lblBenefits.text = "CONOCE LOS BENEFICIOS"
        // MARK: - Image Benefits
        ivBenefitsArrow.image = .icArrowDown
        ivBenefitsArrow.set(rotationFactor: 1.5)
        ivBenefitsArrow.tintColor = .projectYellow
        // MARK: - View container promotions
        vPromotions.cornerRadius = 5
        vPromotions.set(shadow: LabShadow())
        // MARK: - View vertical promotions
        vVerticalPromotions.cornerRadius = 5
        vVerticalPromotions.set(cornerType: .left)
        vVerticalPromotions.backgroundColor = .projectYellow
        // MARK: - ImageView vertical promotion
        ivVerticalPromotions.image = .imgVerticalPromotions
        // MARK: - Label promotion name
        lblPromotionProduct.font = .labFont(with: 14, and: .bold)
        lblPromotionProduct.textColor = .black
        lblPromotionProduct.numberOfLines = 2
        // MARK: - Label previous price
        lblPreviousPrice.font = .labFont(with: 12)
        lblPreviousPrice.textColor = .projectGray
        // MARK: - Label promotion price
        lblPromotionPrice.font = .labFont(with: 12, and: .bold)
        lblPromotionPrice.textColor = .projectRed
        // MARK: - Loading news
        vLoadingNews.set(border: LabBorder(color: .black))
        aiNews.color = .projectRed
    }

    override func setupBinding() {
        viewModel.displayGreetings.bind(to: self) { (self, greetings) in
            self.lblGreetings.text = greetings
        }
        viewModel.displayPromotion.bind(to: self) { (self, _) in
            self.vPromotions.isHidden = false
            self.lcSpacingPromotionsNews.priority = .required
            self.ivPromotions.backgroundColor = .black
            self.lblPromotionProduct.text = "LAB TO SCHOOL - PROTEIN COOKIES 2"
            self.lblPreviousPrice.setTextStrikethrough("antes S/ 20.00", in: .init(location: 6, length: 8))
            self.lblPromotionPrice.text = "ahora S/ 15.00"
        }
        viewModel.displayNews.bind(to: self) { (self, arrNews) in
            self.vLoadingNews.isHidden = true
            self.aiNews.stopAnimating()
            self.arrNews = arrNews
            self.cvNews.reloadData()
        }
    }

    // MARK: - Actions
    @objc func actionPromotions() {
        coordinator.showPromotionsFlow()
    }

    @IBAction func actionBeneftis() {
        coordinator.showBenefits()
    }

    @IBAction func actionSectionPromotions() {
        coordinator.showPromotionsFlow()
    }

}

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrNews.count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "newsCell",
            for: indexPath
            ) as? HomeNewsCell else {
                return UICollectionViewCell()
        }
        cell.configure(with: arrNews[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        coordinator.showBlog()
    }

}
