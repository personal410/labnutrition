//
//  HomeNewsCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 8/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class HomeNewsCell: UICollectionViewCell {

    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivArrow: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        ivBackground.cornerRadius = 5
        lblTitle.font = .labFont(with: 17)
        lblTitle.textColor = .white
        ivArrow.image = .icArrowDown
        ivArrow.tintColor = .projectRed
        ivArrow.set(rotationFactor: 1.5)
    }

    func configure(with promotion: Any) {
        ivBackground.backgroundColor = .black
        lblTitle.text = "Titulo"
    }

}
