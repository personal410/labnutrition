//
//  BenefitsCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class BenefitsCoordinator: BaseCoordinator {

    override func start() {
        let viewController: BenefitListVC = .instantiate()
        viewController.viewModel = BenefitListVM()
        push(viewController: viewController)
    }

}
