//
//  BenefitListVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol BenefitListVMProtocol {

    var displayBenefits: Observable<[Benefit]> { get set }

    func loadBenefits()

}

class BenefitListVM: BenefitListVMProtocol {

    var displayBenefits = Observable<[Benefit]>()

    func loadBenefits() {
        let arrBenefits = [Benefit.benefit1, Benefit.benefit2, Benefit.benefit1]
        displayBenefits.notify(with: arrBenefits)
    }

}
