//
//  BenefitCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore
import LabCanvas

class BenefitCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var vContainer: UIView!
    @IBOutlet weak var ivBenefit: UIImageView!
    @IBOutlet weak var vBottom: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnWantIt: UIButton!

    // MARK: - Setup
    override func awakeFromNib() {
        super.awakeFromNib()
        vContainer.set(shadow: LabShadow())
        vContainer.cornerRadius = 5
        ivBenefit.set(cornerType: .top)
        ivBenefit.cornerRadius = 5
        vBottom.cornerRadius = 5
        vBottom.set(cornerType: .bottom)
        lblTitle.textColor = .projectRed
        lblTitle.font = .labFont(with: 13)
        lblDescription.textColor = .darkGray
        lblDescription.font = .labFont(with: 13)
        btnWantIt.backgroundColor = .projectRed
        btnWantIt.cornerRadius = 20
        btnWantIt.setTitle("¡Lo quiero!", withFont: .labFont(with: 17), withColor: .white)
    }

    func configure(with benefit: Benefit) {
        ivBenefit.backgroundColor = .black
        lblTitle.text = benefit.title
        lblDescription.text = benefit.description
    }

}
