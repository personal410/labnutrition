//
//  BenefitListVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

class BenefitListVC: BaseVC {

    // MARK: - Outlet
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var tvBenefits: UITableView!

    // MARK: - Props
    var viewModel: BenefitListVMProtocol!
    var arrBenefits: [Benefit] = []

    // MARK: - Views
    let btnSearch = UIButton.create(with: .search)

    // MARK: - Setup
    override func setupView() {
        navigationView.title = "Beneficios"
        navigationView.backButtonIsHidden = true
        navigationView.addTrailing(btnSearch)
    }

    override func setupBinding() {
        viewModel.displayBenefits.bind(to: self) { (self, arrBenefits) in
            self.arrBenefits = arrBenefits
            self.tvBenefits.reloadData()
        }
    }

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadBenefits()
    }

}

extension BenefitListVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrBenefits.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "benefitCell",
            for: indexPath
            ) as? BenefitCell else {
                return UITableViewCell()
        }
        cell.configure(with: arrBenefits[indexPath.row])
        return cell
    }

}
