//
//  PromotionsVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/12/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class PromotionsVC: BaseVC {

    // MARK: - Outlets
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var tvProducts: UITableView!

    // MARK: - Props
    weak var coordinator: PromotionsCoordinator!
    var viewModel: PromotionVMProtocol!
    var arrPromotionItem: [PromotionItem] = []

    // MARK: - Views
    let btnFilter = UIButton.create(with: .filter)
    let btnSearch = UIButton.create(with: .search)

    // MARK: - Setup
    override func setupView() {
        navigationView.title = "Promociones"
        navigationView.addTrailing([btnFilter, btnSearch])
        btnFilter.addTarget(self, action: #selector(actionFilter), for: .touchUpInside)
    }

    override func setupBinding() {
        navigationView.didBackPressed.bind(to: self) { (self, _) in
            self.coordinator.finish()
        }
        viewModel.displayPromotionItems.bind(to: self) { (self, arrPromotionItem) in
            self.arrPromotionItem = arrPromotionItem
            self.tvProducts.reloadData()
        }
    }

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Action
    @objc func actionFilter() {
        coordinator.showFilterFlow()
    }

}

extension PromotionsVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrPromotionItem.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        arrPromotionItem[indexPath.row].height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let promotionItem = arrPromotionItem[indexPath.row]
        switch promotionItem.type {
        case .single(let product):
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "productCell",
                for: indexPath
                ) as? PromotionProductCell else {
                    return UITableViewCell()
            }
            cell.configure(with: product)
            return cell
        case .promotion(let storeSection):
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "promotionsDayCell",
                for: indexPath
                ) as? StorePromotionsSectionCell else {
                    return UITableViewCell()
            }
            cell.configure(with: storeSection)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator.showProductFlow()
    }

}
