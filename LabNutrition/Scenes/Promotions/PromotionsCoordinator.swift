//
//  PromotionsCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import UIKit

final class PromotionsCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<PromotionsCoordinator>()

    override func start() {
        let viewController: PromotionsVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = PromotionVM()
        push(viewController: viewController)
    }

    func showFilterFlow() {
        let navigationController = UINavigationController()
        let coordinator = FilterCoordinator(presenter: navigationController)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
        presenter.present(navigationController, animated: true)
    }

    func showProductFlow() {
        let navigationController = UINavigationController()
        let coordinator = ProductCoordinator(presenter: navigationController)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
        presenter.present(navigationController, animated: true)
    }

    func finish() {
        presenter.popViewController(animated: true)
        didFinish.notify(with: self)
    }

}
