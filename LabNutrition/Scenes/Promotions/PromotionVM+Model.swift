//
//  PromotionVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

struct PromotionItem {

    let type: PromotionItemType

    var height: CGFloat { type.height }

}

extension PromotionItem {

    enum PromotionItemType {

        case single(ProductViewModel)
        case promotion(StoreSection)

        var height: CGFloat {
            switch self {
            case .single:
                return 150
            case .promotion:
                return 180
            }
        }

    }

}

struct ProductViewModel {

    // MARK: - Props
    let product: Product

    var codigo: String { product.codigo }
    var marca: String { product.marca }
    var descripcion: String { product.descripcion }
    var unidadMedida: String { product.unidadMedida }
    var precioUnitario: Double { product.precioUnitario }
    var fechaCreacion: String { product.fechaCreacion }
    var puntos: String {
        let puntos = max(precioUnitario.rounded(.down), 1)
        return String(format: "%.0f pt%@", puntos, puntos == 1 ? "" : "s")
    }
    var precioPromocion: Double { precioUnitario * 0.9 }

}
