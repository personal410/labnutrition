//
//  PromotionVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol PromotionVMProtocol: BaseVMProtocol {

    var displayPromotionItems: Observable<[PromotionItem]> { get set }

}

class PromotionVM: PromotionVMProtocol {

    var displayPromotionItems = Observable<[PromotionItem]>()

    func loadView() {
        var arrPromotionItem: [PromotionItem] = []
        arrPromotionItem.append(PromotionItem(type: .single(ProductViewModel(product: .productTest))))
        arrPromotionItem.append(PromotionItem(type: .promotion(StoreSection(
            image: .icPromotions,
            title: "PROMOCIÓN DEL DÍA",
            type: .promotion(Promotion.promotionTest)
        ))))
        arrPromotionItem.append(PromotionItem(type: .single(ProductViewModel(product: .productTest2))))
        arrPromotionItem.append(PromotionItem(type: .single(ProductViewModel(product: .productTest))))
        displayPromotionItems.notify(with: arrPromotionItem)
    }

}
