//
//  LogInVM+SocialMedia.swift
//  LabNutrition
//
//  Created by Victor Salazar on 3/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import LabCore
import LabCanvas

extension LogInVM: GIDSignInDelegate {

    func logInWithFB() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: nil) { (result) in
            switch result {
            case .success(granted: let permissions, _, token: let token):
                for permission in permissions {
                    print("permission: \(permission.name)")
                }
                print("success with token: \(token.userID)")
                let request = GraphRequest(
                    graphPath: "me",
                    parameters: ["fields": "id,email,first_name,last_name"],
                    tokenString: token.tokenString,
                    version: nil,
                    httpMethod: .get
                )
                request.start { (_, graphResult, error) in
                    self.displayLoading.notify(with: false)
                    guard
                        error == nil,
                        let dicResult = graphResult as? [String: String],
                        let fbId = dicResult["id"],
                        let email = dicResult["email"],
                        let names = dicResult["first_name"],
                        let lastName = dicResult["last_name"] else {
                        self.showErrorFBConnection()
                        return
                    }
                    self.saveUser(
                        userId: fbId,
                        logInMethod: .facebook,
                        email: email,
                        names: names,
                        lastname: lastName
                    )
                }
            case .failed:
                self.displayLoading.notify(with: false)
                self.showErrorFBConnection()
            case .cancelled:
                self.displayLoading.notify(with: false)
                let alertInformation = AlertInformation(
                    title: "Facebook",
                    message: "Se cancelo la conexión con la cuenta de Facebook"
                )
                self.displayAlert.notify(with: alertInformation)
            }
        }
    }

    func logInWithGoogle() {
        GIDSignIn.sharedInstance()?.signIn()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.displayLoading.notify(with: false)
        guard error == nil, let googleUser = signIn.currentUser, let googleProfile = googleUser.profile else {
            print("error: \(error.localizedDescription)")
            let alertInformation = AlertInformation(
                title: "Google",
                message: "Hubo un error al conectar con la cuenta de Google"
            )
            self.displayAlert.notify(with: alertInformation)
            return
        }
        self.saveUser(
            userId: googleUser.userID,
            logInMethod: .gmail,
            email: googleProfile.email,
            names: googleProfile.givenName,
            lastname: googleProfile.familyName
        )
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("LogInVM didDisconnectWith")
    }

    private func showErrorFBConnection() {
        let alertInformation = AlertInformation(
            title: "Facebook",
            message: "Hubo un error al conectar con la cuenta de Facebook"
        )
        displayAlert.notify(with: alertInformation)
    }

}
