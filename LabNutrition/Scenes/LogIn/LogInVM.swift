//
//  LogInVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import GoogleSignIn
import LabCanvas

protocol LogInVMProtocol: BaseVMProtocol {

    var didLogIn: Observable<Bool> { get set }
    var displayLoading: Observable<Bool> { get set }
    var displayAlert: Observable<AlertInformation> { get set }
    var displayFBButton: Observable<LogInMethodViewModel> { get set }
    var displayGoogleButton: Observable<LogInMethodViewModel> { get set }

    func logIn(with logInMethod: LogInMethod)

}

class LogInVM: NSObject, LogInVMProtocol {

    var didLogIn = Observable<Bool>()
    var displayLoading = Observable<Bool>()
    var displayAlert = Observable<AlertInformation>()
    var displayFBButton = Observable<LogInMethodViewModel>()
    var displayGoogleButton = Observable<LogInMethodViewModel>()

    override init() {
        super.init()
        GIDSignIn.sharedInstance()?.delegate = self
    }

    func loadView() {
        displayFBButton.notify(with: LogInMethodViewModel(logInMethod: .facebook))
        displayGoogleButton.notify(with: LogInMethodViewModel(logInMethod: .gmail))
    }

    func logIn(with logInMethod: LogInMethod) {
        displayLoading.notify(with: true)
        switch logInMethod {
        case .facebook:
            logInWithFB()
        case .gmail:
            logInWithGoogle()
        }
    }

    func saveUser(
        userId: String,
        logInMethod: LogInMethod,
        email: String,
        names: String,
        lastname: String
    ) {
        Session.shared.saveUser(
            userId: userId,
            logInMethod: logInMethod,
            email: email,
            names: names,
            lastname: lastname
        )
        didLogIn.notify(with: true)
    }

    func save(user: User) {
        Session.shared.saveUser(user)
        didLogIn.notify(with: true)
    }

    private func processLogIn(with response: LogInResponse) {
        //        CoreRequestManager.shared.load(LogInRequest()) { [weak self] result in
        //            guard let self = self else { return }
        //            switch result {
        //            case .success(let response):
        //                self.processLogIn(with: response)
        //            case .failure(let error):
        //                print("error: \(error.localizedDescription)")
        //                self.didLogIn.notify(with: true)
        //            }
        //        }
        print("response: \(response.name) \(response.lastName)")
        didLogIn.notify(with: true)
    }

}
