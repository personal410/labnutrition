//
//  IdentificationCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

final class IdentificationCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<IdentificationCoordinator>()

    override func start() {
        let logInVC: LogInVC = .instantiate()
        logInVC.viewModel = LogInVM()
        logInVC.coordinator = self
        push(viewController: logInVC)
    }

    func didLoginSuccessfully() {
        self.didFinish.notify(with: self)
    }

}
