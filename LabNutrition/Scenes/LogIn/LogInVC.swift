//
//  LogInVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/8/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit
import GoogleSignIn
import LabCanvas

class LogInVC: BaseVC {

    // MARK: - VM
    var viewModel: LogInVM!
    weak var coordinator: IdentificationCoordinator!

    // MARK: - Outlets
    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var lblLoginWith: UILabel!
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!

    // MARK: - Setup
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
        // Google
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }

    override func setupView() {
        ivBackground.image = .bgGirl
        ivLogo.image = .imgLab
        lblLoginWith.text = "Iniciar sesión con:"
        lblLoginWith.font = .labFont()
    }

    override func setupBinding() {
        viewModel.displayFBButton.bind(to: self) { (self, logInMethod) in
            self.configure(button: self.btnFB, with: logInMethod)
        }
        viewModel.displayGoogleButton.bind(to: self) { (self, logInMethod) in
            self.configure(button: self.btnGoogle, with: logInMethod)
        }
        viewModel.didLogIn.bind(to: self) { (self, _) in
            self.coordinator.didLoginSuccessfully()
        }
        viewModel.displayLoading.bind(to: self) { (_, display) in
            if display {
                ProgressHUDView.showProgress()
            } else {
                ProgressHUDView.dismiss()
            }
        }
        viewModel.displayAlert.bind(to: self) { (self, alertInformation) in
            self.showAlert(with: alertInformation)
        }
    }

    // MARK: - Actions
    @IBAction func logInWithFB() {
        viewModel.logIn(with: .facebook)
    }

    @IBAction func logInWithGmail() {
        viewModel.logIn(with: .gmail)
    }

    // MARK: - Auxiliar
    private func configure(button: UIButton, with logInMethod: LogInMethodViewModel) {
        button.backgroundColor = .white
        button.cornerRadius = 20
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        button.setImage(logInMethod.image, for: .normal)
        button.setTitle(logInMethod.title, withFont: .labFont(with: 18), withColor: logInMethod.color)
    }

}
