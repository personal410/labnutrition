//
//  LogInVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

struct LogInMethodViewModel {

    // MARK: - Props
    let logInMethod: LogInMethod
    var title: String {
        switch logInMethod {
        case .facebook:
            return "Facebook"
        case .gmail:
            return "Google"
        }
    }
    var image: UIImage {
        switch logInMethod {
        case .facebook:
            return .icFB
        case .gmail:
            return .icGoogle
        }
    }
    var color: UIColor {
        switch logInMethod {
        case .facebook:
            return .projectBlue
        case .gmail:
            return .black
        }
    }

}
