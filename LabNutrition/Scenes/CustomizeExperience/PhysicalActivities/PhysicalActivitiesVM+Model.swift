//
//  PhysicalActivitiesVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 29/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

struct PhysicalActivityViewModel {

    var physicalActivity: PhysicalActivity
    var name: String { physicalActivity.name }
    var isSelected: Bool { physicalActivity.isSelected }

}
