//
//  PhysicalActivitiesVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 26/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class PhysicalActivitiesVC: CustomizeExperienceBaseVC {

    // MARK: - Props
    var viewModel: PhysicalActivitiesVMProtocol!
    var arrPhysicalActivity: [PhysicalActivityViewModel] = []

    // MARK: - Outlets
    @IBOutlet weak var tvPhysicalActivity: UITableView!

    // MARK: - Setup
    override func setupView() {
        super.setupView()
        pageControl.currentPage = 1
        ivBackground.image = .bgGirl
        lblTitle.text = "Marca las actividades físicas y/o deportes que practicas"
    }

    override func setupBinding() {
        viewModel.displayBackButton.bind(to: self) { (self, show) in
            self.btnBack.isHidden = !show
        }
        viewModel.displayPhysicalActivities.bind(to: self) { (self, physicalActivities) in
            self.arrPhysicalActivity = physicalActivities
            self.tvPhysicalActivity.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionContinue() {
        viewModel.savePhysicalActivities(arrPhysicalActivity)
        coordinator.showTrainingLevel()
    }

}

extension PhysicalActivitiesVC: LabTableViewProtocol {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrPhysicalActivity.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "physicalActivityCell",
            for: indexPath
            ) as? PhysicalActivityTableViewCell else {
                return UITableViewCell()
        }
        let physicalActivity = arrPhysicalActivity[indexPath.row]
        cell.configure(with: physicalActivity)
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let physicalActivity = arrPhysicalActivity[indexPath.row]
        if physicalActivity.isSelected {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        arrPhysicalActivity[indexPath.row].physicalActivity.isSelected = true
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        arrPhysicalActivity[indexPath.row].physicalActivity.isSelected = false
    }

}
