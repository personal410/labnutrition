//
//  PhysicalActivitiesVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol PhysicalActivitiesVMProtocol: CustomizeExperienceBaseVMProtocol {

    var displayPhysicalActivities: Observable<[PhysicalActivityViewModel]> { get set }

    func savePhysicalActivities(_ arrPhysicalActivityViewModel: [PhysicalActivityViewModel])

}

class PhysicalActivitiesVM: CustomizeExperienceBaseVM, PhysicalActivitiesVMProtocol {

    var displayPhysicalActivities = Observable<[PhysicalActivityViewModel]>()

    override func loadView() {
        super.loadView()
        guard let profile = Session.shared.profile else { return }
        let arrPhysicalActivities = profile.arrPhysicalActivities.map { PhysicalActivityViewModel(physicalActivity: $0)
        }
        displayPhysicalActivities.notify(with: arrPhysicalActivities)
    }

    func savePhysicalActivities(_ arrPhysicalActivityViewModel: [PhysicalActivityViewModel]) {
        let arrPhysicalActivity = arrPhysicalActivityViewModel.map {
            PhysicalActivity(name: $0.name, isSelected: $0.isSelected)
        }
        Session.shared.updatePhysicalActivities(arrPhysicalActivity)
    }

}
