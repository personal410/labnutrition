//
//  PhysicalActivityTableViewCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 7/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class PhysicalActivityTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var ivChk: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    // MARK: - Cell
    override func awakeFromNib() {
        super.awakeFromNib()
        ivChk.tintColor = .white
        lblName.textColor = .white
        lblName.font = .labFont(with: 18)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivChk.image = isSelected ? .icChecked : .icUnchecked
    }

    // MARK: - Auxiliar
    func configure(with physicalActivityViewModel: PhysicalActivityViewModel) {
        lblName.text = physicalActivityViewModel.name
        setSelected(physicalActivityViewModel.isSelected, animated: false)
    }

}
