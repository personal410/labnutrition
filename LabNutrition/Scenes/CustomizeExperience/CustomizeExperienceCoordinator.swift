//
//  CustomizeExperienceCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

final class CustomizeExperienceCoordinator: BaseCoordinator, FinishCoordinator {

    // MARK: - Props
    var didFinish = Observable<CustomizeExperienceCoordinator>()
    private var isFirstLogIn: Bool

    // MARK: - Init
    init(presenter: UINavigationController, isFirstLogIn: Bool = false) {
        presenter.modalPresentationStyle = .fullScreen
        presenter.navigationBar.isHidden = true
        self.isFirstLogIn = isFirstLogIn
        super.init(presenter: presenter)
    }

    // MARK: - Flow
    override func start() {
        if isFirstLogIn {
            let viewController: WelcomeVC = .instantiate()
            viewController.coordinator = self
            push(viewController: viewController)
        } else {
            showPersonalInfo()
        }
    }

    func finish() {
        presenter.dismiss(animated: true)
        didFinish.notify(with: self)
    }

    func showPersonalInfo() {
        let viewController: PersonalInfoVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = PersonalInfoVM(isFirstLogin: isFirstLogIn)
        push(viewController: viewController)
    }

    func showPhysicalActivities() {
        let viewController: PhysicalActivitiesVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = PhysicalActivitiesVM(isFirstLogin: isFirstLogIn)
        push(viewController: viewController)
    }

    func showTrainingLevel() {
        let viewController: TrainingLevelVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = TrainingLevelVM(isFirstLogin: isFirstLogIn)
        push(viewController: viewController)
    }

    func showObjectiveList() {
        let viewController: ObjectiveListVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = ObjectiveListVM(isFirstLogin: isFirstLogIn)
        push(viewController: viewController)
    }

}
