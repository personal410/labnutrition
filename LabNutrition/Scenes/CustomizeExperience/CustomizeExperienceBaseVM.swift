//
//  CustomizeExperienceBaseVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol CustomizeExperienceBaseVMProtocol: BaseVMProtocol {

    var displayBackButton: Observable<Bool> { get set }

}

class CustomizeExperienceBaseVM: CustomizeExperienceBaseVMProtocol {

    let isFirstLogIn: Bool
    var displayBackButton = Observable<Bool>()

    init(isFirstLogin: Bool) {
        self.isFirstLogIn = isFirstLogin
    }

    func loadView() {
        displayBackButton.notify(with: !isFirstLogIn)
    }

}
