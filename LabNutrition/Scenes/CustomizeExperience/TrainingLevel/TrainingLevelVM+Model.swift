//
//  TrainingLevelVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

struct TrainingLevelViewModel {

    let trainingLevel: TrainingLevel

    var title: String {
        switch trainingLevel {
        case .beginner:
            return "Principiante"
        case .intermediate:
            return "Intermedio"
        case .advanced:
            return "Avanzado"
        }
    }

    var description: String {
        switch trainingLevel {
        case .beginner:
            return "Pocas veces al mes"
        case .intermediate:
            return "Algunos días de la semana"
        case .advanced:
            return "Entrenamiento diario"
        }
    }

}
