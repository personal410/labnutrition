//
//  TrainingLevelVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol TrainingLevelVMProtocol: CustomizeExperienceBaseVMProtocol {

    var displayTrainingLevels: Observable<[TrainingLevelViewModel]> { get set }
    var displaySelectedTrainingLevel: Observable<Int> { get set }

    func saveTrainingLevel(with index: Int)

}

class TrainingLevelVM: CustomizeExperienceBaseVM, TrainingLevelVMProtocol {

    private let arrTrainingLevel: [TrainingLevel] = TrainingLevel.all
    var displayTrainingLevels = Observable<[TrainingLevelViewModel]>()
    var displaySelectedTrainingLevel = Observable<Int>()

    override func loadView() {
        super.loadView()
        let arrTrainingLevelViewModel = arrTrainingLevel.map {
            TrainingLevelViewModel(trainingLevel: $0)
        }
        displayTrainingLevels.notify(with: arrTrainingLevelViewModel)
        guard let profile = Session.shared.profile else { return }
        guard let index = arrTrainingLevel.firstIndex(of: profile.trainingLevel) else { return }
        displaySelectedTrainingLevel.notify(with: index)
    }

    func saveTrainingLevel(with index: Int) {
        Session.shared.updateTrainingLevel(arrTrainingLevel[index])
    }

}
