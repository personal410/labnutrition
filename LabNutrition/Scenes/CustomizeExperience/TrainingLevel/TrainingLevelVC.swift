//
//  TrainingLevelVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class TrainingLevelVC: CustomizeExperienceBaseVC {

    // MARK: - Props
    var viewModel: TrainingLevelVMProtocol!
    var arrTrainingLevel: [TrainingLevelViewModel] = []

    // MARK: - Outlets
    @IBOutlet weak var tvTrainingLevel: UITableView!

    // MARK: - Setup
    override func setupView() {
        super.setupView()
        ivBackground.image = .bgBoy
        lblTitle.text = "Nivel de entrenamiento"
        pageControl.currentPage = 2
    }

    override func setupBinding() {
        viewModel.displayBackButton.bind(to: self) { (self, show) in
            self.btnBack.isHidden = !show
        }
        viewModel.displayTrainingLevels.bind(to: self) { (self, arrTrainingLevel) in
            self.arrTrainingLevel = arrTrainingLevel
            self.tvTrainingLevel.reloadData()
        }
        viewModel.displaySelectedTrainingLevel.bind(to: self) { (self, selectedIndex) in
            self.tvTrainingLevel.selectRow(
                at: IndexPath(row: selectedIndex, section: 0),
                animated: false,
                scrollPosition: .none
            )
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionContinue() {
        guard let index = tvTrainingLevel.indexPathForSelectedRow?.row else { return }
        viewModel.saveTrainingLevel(with: index)
        coordinator.showObjectiveList()
    }

}

extension TrainingLevelVC: LabTableViewProtocol {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrTrainingLevel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "trainingLevelCell",
            for: indexPath
            ) as? TrainingLevelCell else {
                return UITableViewCell()
        }
        let viewModel = arrTrainingLevel[indexPath.row]
        cell.configure(with: viewModel)
        return cell
    }

}
