//
//  TrainingLevelCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class TrainingLevelCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var vBorder: UIView!
    @IBOutlet weak var lblTile: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var ivCheck: UIImageView!

    // MARK: - Setup
    override func awakeFromNib() {
        super.awakeFromNib()
        vBorder.set(border: LabBorder(color: .white))
        vBorder.cornerRadius = 5
        lblTile.font = .labFont(with: 20)
        lblTile.textColor = .white
        lblDescription.font = .labFont(with: 16)
        lblDescription.textColor = .white
        ivCheck.tintColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivCheck.image = isSelected ? .icCircleChecked : .icCircleUnchecked
    }

    func configure(with viewModel: TrainingLevelViewModel) {
        lblTile.text = viewModel.title
        lblDescription.text = viewModel.description
    }

}
