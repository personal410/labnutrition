//
//  CustomizeExperienceBaseVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class CustomizeExperienceBaseVC: BaseVC {

    // MARK: - Props
    weak var coordinator: CustomizeExperienceCoordinator!

    // MARK: - Outlets
    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!

    // MARK: - Setup
    override func setupView() {
        btnBack.tintColor = .white
        btnBack.setImage(.icBack, for: .normal)
        lblTitle.numberOfLines = 0
        lblTitle.font = .labFont(with: 18)
        lblTitle.textColor = .white
        btnContinue.backgroundColor = .projectBlack3
        btnContinue.cornerRadius = 20
        btnContinue.setTitle("Continuar", withFont: .labFont(with: 18), withColor: .white)
        pageControl.numberOfPages = 4
    }

}
