//
//  PersonalInfoVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class PersonalInfoVC: CustomizeExperienceBaseVC {

    // MARK: - Props
    var viewModel: PersonalInfoVMProtocol!

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tfAge: ImageTextField!
    @IBOutlet weak var cbGender: ComboBox!
    @IBOutlet weak var tfHeight: ImageTextField!
    @IBOutlet weak var tfWeight: ImageTextField!
    @IBOutlet weak var btnFillAfter: UIButton!

    // MARK: - Setup
    override func setupView() {
        super.setupView()
        ivBackground.image = .bgBoy
        lblTitle.text = "Ayúdanos a personalizar tu experiencia"
        configure(imageTextField: tfAge)
        tfAge.set(placeholder: "Edad", withColor: .white)
        configure(imageTextField: cbGender)
        cbGender.defaultValue = "Sexo"
        cbGender.set(placeholder: "Sexo", withColor: .white)
        configure(imageTextField: tfHeight)
        tfHeight.set(placeholder: "Altura", withColor: .white)
        configure(imageTextField: tfWeight)
        tfWeight.set(placeholder: "Peso", withColor: .white)
        btnFillAfter.setTitle("Completar más tarde", withFont: .labFont(with: 18), withColor: .white)
        pageControl.currentPage = 0
    }

    override func setupBinding() {
        viewModel.displayBackButton.bind(to: self) { (self, show) in
            self.btnBack.isHidden = !show
        }
        viewModel.displayAge.bind(to: self) { (self, age) in
            self.tfAge.text = age
        }
        viewModel.loadGenders.bind(to: self) { (self, genders) in
            self.cbGender.arrValues = genders
        }
        viewModel.displayGender.bind(to: self) { (self, gender) in
            self.cbGender.text = gender
        }
        viewModel.displayHeight.bind(to: self) { (self, height) in
            self.tfHeight.text = height
        }
        viewModel.displayWeight.bind(to: self) { (self, height) in
            self.tfWeight.text = height
        }
        viewModel.displayFillAfterButton.bind(to: self) { (self, show) in
            self.btnFillAfter.isHidden = !show
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Action
    @IBAction func actionBack() {
        coordinator.finish()
    }

    @IBAction func actionFillAfter() {
        coordinator.finish()
    }

    @IBAction func actionContinue() {
        viewModel.savePersonalInfo(
            newAge: tfAge.text!,
            newGender: cbGender.text!,
            newHeight: tfHeight.text!,
            newWeight: tfWeight.text!
        )
        coordinator.showPhysicalActivities()
    }

    // MARK: - Auxiliars
    private func configure(imageTextField: ImageTextField) {
        imageTextField.textColor = .white
        imageTextField.font = .labFont(with: 14)
        imageTextField.lineColor = .white
    }

}
