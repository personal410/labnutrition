//
//  PersonalInfoVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 29/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol PersonalInfoVMProtocol: CustomizeExperienceBaseVMProtocol {

    var displayAge: Observable<String> { get set }
    var loadGenders: Observable<[String]> { get set }
    var displayGender: Observable<String> { get set }
    var displayHeight: Observable<String> { get set }
    var displayWeight: Observable<String> { get set }
    var displayFillAfterButton: Observable<Bool> { get set }

    func savePersonalInfo(newAge: String, newGender: String, newHeight: String, newWeight: String)

}

class PersonalInfoVM: CustomizeExperienceBaseVM, PersonalInfoVMProtocol {

    var displayAge = Observable<String>()
    var loadGenders = Observable<[String]>()
    var displayGender = Observable<String>()
    var displayHeight = Observable<String>()
    var displayWeight = Observable<String>()
    var displayFillAfterButton = Observable<Bool>()

    override func loadView() {
        super.loadView()
        displayFillAfterButton.notify(with: isFirstLogIn)
        guard let profile = Session.shared.profile else { return }
        loadGenders.notify(with: ["Hombre", "Mujer"])
        displayAge.notify(with: profile.age == -1 ? "" : "\(profile.age)")
        displayGender.notify(with: profile.gender)
        displayHeight.notify(with: profile.height == -1 ? "" : profile.height.textTwoDecimals)
        displayWeight.notify(with: profile.weight == -1 ? "" : profile.weight.textTwoDecimals)
    }

    func savePersonalInfo(newAge: String, newGender: String, newHeight: String, newWeight: String) {
        let age = Int(newAge) ?? 0
        let height = Double(newHeight) ?? 0
        let weight = Double(newWeight) ?? 0
        let finalAge = age == 0 ? -1 : age
        let finalHeight = height == 0 ? -1 : height
        let finalWeight = weight == 0 ? -1 : weight
        Session.shared.updatePersonalInfo(
            age: finalAge,
            gender: newGender,
            height: finalHeight,
            weight: finalWeight
        )
    }

}
