//
//  ObjectiveListVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol ObjectiveListVMProtocol: CustomizeExperienceBaseVMProtocol {

    var displayObjectiveList: Observable<[ObjectiveViewModel]> { get set }
    var displaySelectedObjectives: Observable<[IndexPath]> { get set }

    func saveObjectives(with arrIndex: [Int])

}

class ObjectiveListVM: CustomizeExperienceBaseVM, ObjectiveListVMProtocol {

    private let arrObjective = Objective.all
    var displayObjectiveList = Observable<[ObjectiveViewModel]>()
    var displaySelectedObjectives = Observable<[IndexPath]>()

    override func loadView() {
        super.loadView()
        let arrObjectiveModel = arrObjective.map { ObjectiveViewModel(objective: $0) }
        displayObjectiveList.notify(with: arrObjectiveModel)
        guard let profile = Session.shared.profile else { return }
        var arrSelectedIndex: [IndexPath] = []
        for objectiveId in profile.arrObjectiveIds {
            if let index = arrObjective.firstIndex(where: { (objective) -> Bool in
                objective.objectiveId == objectiveId
            }) {
                arrSelectedIndex.append(IndexPath(row: index, section: 0))
            }
        }
        displaySelectedObjectives.notify(with: arrSelectedIndex)
    }

    func saveObjectives(with arrIndex: [Int]) {
        var arrObjectivesIds: [Int] = []
        for index in arrIndex {
            arrObjectivesIds.append(arrObjective[index].objectiveId)
        }
        Session.shared.updateObjectiveIds(arrObjectivesIds)
    }

}
