//
//  ObjectiveListVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ObjectiveListVC: CustomizeExperienceBaseVC {

    // MARK: - Props
    var viewModel: ObjectiveListVMProtocol!
    var arrObjective: [ObjectiveViewModel] = []

    // MARK: - Outlets
    @IBOutlet weak var tvObjective: UITableView!

    // MARK: - Setup
    override func setupView() {
        super.setupView()
        ivBackground.image = .bgGirl
        lblTitle.text = "Tus objectivos"
        btnContinue.setTitle("Finalizar", withFont: .labFont(with: 18), withColor: .white)
        pageControl.currentPage = 3
    }

    override func setupBinding() {
        viewModel.displayBackButton.bind(to: self) { (self, show) in
            self.btnBack.isHidden = !show
        }
        viewModel.displayObjectiveList.bind(to: self) { (self, objectives) in
            self.arrObjective = objectives
            self.tvObjective.reloadData()
        }
        viewModel.displaySelectedObjectives.bind(to: self) { (self, indexPaths) in
            for indexPath in indexPaths {
                self.tvObjective.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionFinish() {
        var arrIndex: [Int] = []
        if let indexPaths = tvObjective.indexPathsForSelectedRows {
            arrIndex = indexPaths.map { $0.row }
        }
        viewModel.saveObjectives(with: arrIndex)
        coordinator.finish()
    }

}

extension ObjectiveListVC: LabTableViewProtocol {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrObjective.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "objectiveCell",
            for: indexPath
            ) as? ObjectiveCell else {
                return UITableViewCell()
        }
        cell.configure(with: arrObjective[indexPath.row])
        return cell
    }

}
