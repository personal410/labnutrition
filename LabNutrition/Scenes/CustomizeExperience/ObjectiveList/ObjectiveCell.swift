//
//  ObjectiveCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 8/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ObjectiveCell: UITableViewCell {

    var style: Style = .white {
        didSet {
            setupColors()
        }
    }

    // MARK: - Outlets
    @IBOutlet weak var ivObjective: UIImageView!
    @IBOutlet weak var lblObjective: UILabel!
    @IBOutlet weak var ivSwitch: UIImageView!

    // MARK: - Setup
    override func awakeFromNib() {
        super.awakeFromNib()
        lblObjective.font = .labFont(with: 13)
        ivSwitch.image = .icSwitch
        setupColors()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivSwitch.tintColor = isSelected ? style.selectedSwitchColor : style.unselectedSwitchColor
        ivSwitch.transform = isSelected ? .identity : .init(rotationAngle: .pi)
    }

    func configure(with model: ObjectiveViewModel) {
        ivObjective.image = model.image
        lblObjective.text = model.name
    }

    private func setupColors() {
        ivObjective.tintColor = style.tintColor
        lblObjective.textColor = style.tintColor
    }

}

extension ObjectiveCell {

    enum Style {

        case white
        case lightGray

        var tintColor: UIColor {
            switch self {
            case .white:
                return .white
            case .lightGray:
                return .lightGray
            }
        }

        var unselectedSwitchColor: UIColor {
            switch self {
            case .white:
                return UIColor.white.withAlphaComponent(0.75)
            case .lightGray:
                return .lightGray
            }
        }

        var selectedSwitchColor: UIColor {
            switch self {
            case .white:
                return .white
            case .lightGray:
                return .projectBlue
            }
        }

    }

}
