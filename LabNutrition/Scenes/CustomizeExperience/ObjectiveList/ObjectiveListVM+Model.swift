//
//  ObjectiveListVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 7/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import UIKit

struct ObjectiveViewModel {

    let objective: Objective

    var name: String { objective.description.uppercased() }
    var image: UIImage {
        switch objective.objectiveId {
        case 1:
            return .icArm
        case 2:
            return .icLighting
        case 3:
            return .icHeart
        case 4:
            return .icWaist
        case 5:
            return .icChronometer
        case 6:
            return .icChest
        case 7:
            return .icGymWeight
        default:
            return .icArm
        }
    }

}
