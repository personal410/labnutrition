//
//  WelcomeVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class WelcomeVC: BaseVC {

    // MARK: - Outlet
    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var vTop: UIView!
    @IBOutlet weak var vBottom: UIView!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblMesage: UILabel!
    @IBOutlet weak var btnContinue: UIButton!

    // MARK: - Props
    weak var coordinator: CustomizeExperienceCoordinator!

    // MARK: - Setup
    override func setupView() {
        ivBackground.contentMode = .scaleAspectFill
        ivBackground.image = .bgGirl
        vTop.backgroundColor = .clear
        lblWelcome.text = "¡Bienvenido!"
        lblWelcome.textColor = .white
        lblWelcome.font = .labFont(with: 50)
        lblWelcome.textAlignment = .center
        lblMesage.text = "Tu cuenta a sido creada con éxito"
        lblMesage.textColor = .white
        lblMesage.numberOfLines = 2
        lblMesage.textAlignment = .center
        lblMesage.font = .labFont(with: 25)
        vBottom.backgroundColor = .clear
        btnContinue.backgroundColor = .projectBlack3
        btnContinue.cornerRadius = 20
        btnContinue.setTitle("Continuar", withFont: .labFont(with: 18), withColor: .white)
    }

    // MARK: - Action
    @IBAction func actionContinue() {
        coordinator.showPersonalInfo()
    }

}
