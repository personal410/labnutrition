//
//  SuccessfulPurchaseVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class SuccessfulPurchaseVC: BaseVC {

    // MARK: - Props
    weak var coordinator: ShoppingCartCoordinator!

    // MARK: - Outlet
    @IBOutlet weak var vAd: UIView!

    // MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.vAd.isHidden = true
        }
    }

    // MARK: - Action
    @IBAction func actionAccept() {
        navigationController?.popToRootViewController(animated: true)
    }

}
