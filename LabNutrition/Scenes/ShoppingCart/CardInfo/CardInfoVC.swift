//
//  CardInfoVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class CardInfoVC: BaseVC {

    // MARK: - Props
    weak var coordinator: ShoppingCartCoordinator!

    // MARK: - Outlets
    @IBOutlet weak var svContent: UIScrollView!
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfMonthYear: UITextField!
    @IBOutlet weak var tfCVV: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfEmail: UITextField!

    // MARK: - Setup
    override func setupView() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showKeyboard(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willHideKeyboard),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    // MARK: - Action
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionHideKeyboard() {
        tfCardNumber.resignFirstResponder()
        tfMonthYear.resignFirstResponder()
        tfCVV.resignFirstResponder()
        tfAddress.resignFirstResponder()
        tfCity.resignFirstResponder()
        tfPhoneNumber.resignFirstResponder()
        tfEmail.resignFirstResponder()
    }

    @IBAction func actionContinue() {
        coordinator.showPurchaseSummary()
    }

    // MARK: - Keyboard
    @objc func showKeyboard(_ notification: Notification) {
        guard
            let info = notification.userInfo,
            let keyboardFrameValue = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight = keyboardFrameValue.cgRectValue.height
        let newEdgeInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight - 110, right: 0)
        svContent.contentInset = newEdgeInset
        svContent.scrollIndicatorInsets = newEdgeInset
    }

    @objc func willHideKeyboard() {
        let newEdgeInset = UIEdgeInsets.zero
        svContent.contentInset = newEdgeInset
        svContent.scrollIndicatorInsets = newEdgeInset
    }

}
