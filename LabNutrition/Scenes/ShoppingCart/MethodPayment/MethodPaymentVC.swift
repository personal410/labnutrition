//
//  MethodPaymentVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class MethodPaymentVC: BaseVC {

    // MARK: - Props
    weak var coordinator: ShoppingCartCoordinator!
    let arrMethods: [String] = ["", ""]

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionNext() {
        coordinator.showCardInfo()
    }

}

extension MethodPaymentVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrMethods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "methodPaymentCell", for: indexPath)
        return cell
    }

}
