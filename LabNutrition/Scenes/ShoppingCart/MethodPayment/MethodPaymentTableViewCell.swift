//
//  MethodPaymentTableViewCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 7/10/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class MethodPaymentTableViewCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var ivSelected: UIImageView!
    @IBOutlet weak var ivMethodPayment: UIImageView!

    // MARK: - Cell
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivSelected.backgroundColor = selected ? .red : .lightGray
    }

}
