//
//  PurchaseSummaryVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class PurchaseSummaryVC: BaseVC {

    // MARK: - Props
    weak var coordinator: ShoppingCartCoordinator!

    // MARK: - Action
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionNext() {
        coordinator.showSuccessfullPurchase()
    }

}
