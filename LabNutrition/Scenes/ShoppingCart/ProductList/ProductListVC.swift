//
//  ProductListVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ProductListVC: BaseVC {

    // MARK: - Props
    let arrProducts: [Any] = [0]
    weak var coordinator: ShoppingCartCoordinator!

    // MARK: - Actions
    @IBAction func actionConfirm() {
        coordinator.showMethodPayment()
    }

    @IBAction func actionKeepBuying() {
        coordinator.keepBuying()
    }

}

extension ProductListVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProducts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scProductCell", for: indexPath)
        return cell
    }

}
