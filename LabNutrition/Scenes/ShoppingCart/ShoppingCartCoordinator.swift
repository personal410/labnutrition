//
//  ShoppingCartCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class ShoppingCartCoordinator: BaseCoordinator {

    override func start() {
        let viewController: ProductListVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func showMethodPayment() {
        let viewController: MethodPaymentVC = .instantiate()
        viewController.coordinator = self
        viewController.hidesBottomBarWhenPushed = true
        push(viewController: viewController)
    }

    func showCardInfo() {
        let viewController: CardInfoVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func showPurchaseSummary() {
        let viewController: PurchaseSummaryVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func showSuccessfullPurchase() {
        let viewController: SuccessfulPurchaseVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func keepBuying() {
        guard let mainCoordinator = parent as? MainCoordinator else { return }
        mainCoordinator.changeTab(with: .store)
    }

}
