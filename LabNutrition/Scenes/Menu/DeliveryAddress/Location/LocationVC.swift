//
//  LocationVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class LocationVC: BaseVC {

    // MARK: - Actions
    @IBAction func actionClose() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionConfirmLocation() {
        navigationController?.popViewController(animated: true)
    }

}
