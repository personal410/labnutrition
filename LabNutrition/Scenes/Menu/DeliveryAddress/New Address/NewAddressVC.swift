//
//  NewAddressVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class NewAddressVC: BaseVC {

    // MARK: - Props
    weak var coordinator: DeliveryAddressCoordinator!

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionGetLocation() {
        coordinator.showGetLocation()
    }

    @IBAction func actionSave() {
        navigationController?.popViewController(animated: true)
    }

}
