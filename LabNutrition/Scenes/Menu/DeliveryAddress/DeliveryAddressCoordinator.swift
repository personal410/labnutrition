//
//  DeliveryAddressCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

final class DeliveryAddressCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<DeliveryAddressCoordinator>()

    override func start() {
        let viewController: DeliveryAddressVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func showAddAddress() {
        let viewController: NewAddressVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func showGetLocation() {
        let viewController: LocationVC = .instantiate()
        push(viewController: viewController)
    }

    func finish() {
        presenter.popViewController(animated: true)
        didFinish.notify(with: self)
    }

}
