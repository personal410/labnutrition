//
//  DeliveryAddressVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class DeliveryAddressVC: BaseVC {

    // MARK: - Props
    weak var coordinator: DeliveryAddressCoordinator!

    // MARK: - Actions
    @IBAction func actionAddAddress() {
        coordinator.showAddAddress()
    }

    @IBAction func actionBack() {
        coordinator.finish()
    }

}

extension DeliveryAddressVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath)
        return cell
    }

}
