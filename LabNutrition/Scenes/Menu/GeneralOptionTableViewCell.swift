//
//  GeneralOptionTableViewCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 8/10/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class GeneralOptionTableViewCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivIcon: UIImageView!

    // MARK: - Configure
    func configure(with menuOption: MenuOption) {
        lblTitle.text = menuOption.key.rawValue
        ivIcon.image = menuOption.image
    }

    func configure(with settingOption: SettingsOption) {
        lblTitle.text = settingOption.key.rawValue
        ivIcon.image = settingOption.image
        ivIcon.tintColor = settingOption.color ?? UIImageView.appearance().tintColor
    }

}
