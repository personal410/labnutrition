//
//  LabCardVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class LabCardVC: BaseVC {

    // MARK: - Setup
    override func setupView() {
        view.backgroundColor = .projectRed
    }

    // MARK: - Action
    @IBAction func actionClose() {
        navigationController?.popViewController(animated: true)
    }

}
