//
//  Notification.swift
//  LabNutrition
//
//  Created by Jorge Mayta Guillermo on 11/7/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import Foundation

struct Information {

    var title: String
    var message: String
    var date: Date

    var hour: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }

}
