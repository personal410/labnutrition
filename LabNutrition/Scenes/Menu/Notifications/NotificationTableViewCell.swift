//
//  TableViewCell.swift
//  LabNutrition
//
//  Created by Jorge Mayta Guillermo on 11/7/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    // MARK: - Outlet

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    // MARK: - Auxiliar
    func bind(with notification: Information) {
        lblTitle.text = notification.title
        lblMessage.text = notification.message
        lblDate.text = notification.hour
    }

}
