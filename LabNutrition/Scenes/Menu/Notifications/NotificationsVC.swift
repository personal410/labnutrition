//
//  NotificationsVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class NotificationsVC: BaseVC {

    // MARK: - Outlet
    @IBOutlet weak var notificationTableView: UITableView!

    // MARK: - Props
    var arrNotifications: [Information] = [
        Information(title: "Tenemos algo para ti", message: "Y es que LAB nutrition pensamos en ti", date: Date()),
        Information(title: "Prueba el nuevo", message: "Y es que LAB nutrition pensamos en ti", date: Date())
    ]

    // MARK: - Actions
    @IBAction func actionClose() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionDeleteAll() {
        arrNotifications.removeAll()
        notificationTableView.reloadData()
    }

}

extension NotificationsVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrNotifications.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "notificationCell",
            for: indexPath) as? NotificationTableViewCell else {
            return UITableViewCell()
        }
        cell.bind(with: arrNotifications[indexPath.row])
        return cell
    }

}
