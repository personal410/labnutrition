//
//  MenuCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class MenuCoordinator: BaseCoordinator {

    override func start() {
        let viewController: MenuVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func handle(_ menuOption: MenuOption) {
        switch menuOption.key {
        case .bonusLab:
            showBonusLab()
        case .notifications:
            showNotifications()
        case .blog:
            showBlogFlow()
        case .myPurchases:
            showMyPurchasesFlow()
        case .labCard:
            showLabCard()
        }
    }

    func showBonusLab() {
        let viewController: BonusLabVC = .instantiate()
        viewController.hidesBottomBarWhenPushed = true
        push(viewController: viewController)
    }

    func showNotifications() {
        let viewController: NotificationsVC = .instantiate()
        viewController.hidesBottomBarWhenPushed = true
        push(viewController: viewController)
    }

    func showBlogFlow() {
        let coordinator = BlogCoordinator(presenter: presenter)
        add(child: coordinator)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        coordinator.start()
    }

    func showMyPurchasesFlow() {
        let coordinator = MyPurchasesCoordinator(presenter: presenter)
        add(child: coordinator)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        coordinator.start()
    }

    func showLabCard() {
        let viewController: LabCardVC = .instantiate()
        viewController.hidesBottomBarWhenPushed = true
        push(viewController: viewController)
    }

    func showSettings() {
        let coordinator = SettingsCoordinator(presenter: presenter)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        coordinator.didCloseSession.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
            self.closeSession()
        }
        add(child: coordinator)
        coordinator.start()
    }

    func handle(_ settingsOption: SettingsOption) {
        switch settingsOption.key {
        case .profile:
            showProfile()
        case .deliveryAddress:
            showDeliveryAddressFlow()
        case .closeSession:
            closeSession()
        default:
            break
        }
    }

    func showProfile() {
        let viewController: ProfileVC = .instantiate()
        push(viewController: viewController)
    }

    func showDeliveryAddressFlow() {
        let coordinator = DeliveryAddressCoordinator(presenter: presenter)
        add(child: coordinator)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        coordinator.start()
    }

    func closeSession() {
        if let parentCoordinator = parent as? MainCoordinator {
            parentCoordinator.finish()
        }
    }

}
