//
//  MyPurchasesCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

final class MyPurchasesCoordinator: BaseCoordinator, FinishCoordinator {

    // MARK: - Props
    var didFinish = Observable<MyPurchasesCoordinator>()

    // MARK: - Lifecycle
    override func start() {
        let viewController: MyPurchasesVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func finish() {
        presenter.popViewController(animated: true)
        didFinish.notify(with: self)
    }

}
