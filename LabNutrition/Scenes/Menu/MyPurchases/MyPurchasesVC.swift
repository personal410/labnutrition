//
//  MyPurchasesVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class MyPurchasesVC: BaseVC {

    // MARK: - Props
    var arrMyPurchases: [String] = [""]
    weak var coordinator: MyPurchasesCoordinator!

    // MARK: - Action
    @IBAction func actionClose() {
        coordinator.finish()
    }

}

extension MyPurchasesVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrMyPurchases.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myPurchaseCell", for: indexPath)
        return cell
    }

}
