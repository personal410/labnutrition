//
//  MenuVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class MenuVC: BaseVC {

    // MARK: - Props
    weak var coordinator: MenuCoordinator!
    let arrMenuOption = MenuOption.menuOptions

    // MARK: - Actions
    @IBAction func actionSettings() {
        coordinator.showSettings()
    }

}

extension MenuVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrMenuOption.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "profileOptionCell",
            for: indexPath) as? GeneralOptionTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: arrMenuOption[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator.handle(arrMenuOption[indexPath.row])
    }

}
