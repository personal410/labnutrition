//
//  SettingsOption.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

struct SettingsOption: GeneralOption {

    static let profile = SettingsOption(key: .profile, image: .icUser)
    static let experience = SettingsOption(key: .experience, image: .icMedal)
    static let notifications = SettingsOption(key: .notifications, image: .imgSwitch, color: .projectBlue)
    static let deliveryAddress = SettingsOption(key: .deliveryAddress, image: .icLocation)
    static let changePassword = SettingsOption(key: .changePassword, image: .icKey)
    static let closeSession = SettingsOption(key: .closeSession, image: .icGetOut)

    static let all: [SettingsOption] = [
        .profile,
        .experience,
        .notifications,
        .deliveryAddress,
        .changePassword,
        .closeSession
    ]

    var key: SettingsOption.Key
    var image: UIImage
    let color: UIColor?

    init(key: SettingsOption.Key, image: UIImage, color: UIColor? = nil) {
        self.key = key
        self.image = image
        self.color = color
    }

}

extension SettingsOption {

    enum Key: String {

        case profile = "Perfil"
        case experience = "Personaliza tu experiencia"
        case notifications = "Activar notificaciones"
        case deliveryAddress = "Dirección de entrega"
        case changePassword = "Cambiar contraseña"
        case closeSession = "Cerrar sesión"

    }

}
