//
//  SettingsVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 26/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {

    // MARK: - Props
    let arrOptions = SettingsOption.all
    var viewModel: SettingsVMProtocol!
    weak var coordinator: SettingsCoordinator!

    // MARK: - Actions
    @IBAction func actionClose() {
        coordinator.finish()
    }

}

extension SettingsVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "profileOptionCell",
            for: indexPath) as? GeneralOptionTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: arrOptions[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator.handle(arrOptions[indexPath.row])
    }

}
