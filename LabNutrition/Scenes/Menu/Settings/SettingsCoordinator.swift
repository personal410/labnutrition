//
//  SettingsCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 30/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import LabCanvas
import UIKit

final class SettingsCoordinator: BaseCoordinator, FinishCoordinator {

    // MARK: - Props
    var didFinish = Observable<SettingsCoordinator>()
    var didCloseSession = Observable<SettingsCoordinator>()

    // MARK: - Lifecycle
    override func start() {
        let viewController: SettingsVC = .instantiate()
        viewController.hidesBottomBarWhenPushed = true
        viewController.coordinator = self
        viewController.viewModel = SettingsVM()
        push(viewController: viewController)
    }

    func handle(_ settingsOption: SettingsOption) {
        switch settingsOption.key {
        case .profile:
            showProfile()
        case .experience:
            showCustomizeExperience()
        case .deliveryAddress:
            showDeliveryAddressFlow()
        case .closeSession:
            closeSession()
        default:
            break
        }
    }

    func showProfile() {
        let viewController: ProfileVC = .instantiate()
        push(viewController: viewController)
    }

    func showCustomizeExperience() {
        let navigationController = UINavigationController()
        let coordinator = CustomizeExperienceCoordinator(presenter: navigationController)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
        presenter.present(navigationController, animated: true)
    }

    func showDeliveryAddressFlow() {
        let coordinator = DeliveryAddressCoordinator(presenter: presenter)
        add(child: coordinator)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        coordinator.start()
    }

    func closeSession() {
        let alertInformation = AlertInformation(
            title: "Cerrar sesión",
            message: "¿Quieres cerrar sesión?",
            okButtonTitle: "Si",
            cancelButtonTitle: "No") {
                Session.shared.closeSession()
                self.didCloseSession.notify(with: self)
        }
        presenter.showAlert(with: alertInformation)
    }

    func finish() {
        presenter.popViewController(animated: true)
        didFinish.notify(with: self)
    }

}
