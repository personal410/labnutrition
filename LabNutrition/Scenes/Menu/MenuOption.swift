//
//  MenuOption.swift
//  LabNutrition
//
//  Created by Victor Salazar on 25/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

struct MenuOption: GeneralOption {

    static let bonusLab = MenuOption(key: .bonusLab, image: .icMedal)
    static let notifications = MenuOption(key: .notifications, image: .icBell)
    static let blog = MenuOption(key: .blog, image: .icBulb)
    static let myPurchases = MenuOption(key: .myPurchases, image: .icBag)
    static let labCard = MenuOption(key: .labCard, image: .icLabcard)

    static let menuOptions: [MenuOption] = [.bonusLab, .notifications, .blog, .myPurchases, .labCard]

    var key: MenuOption.Key
    var image: UIImage

}

extension MenuOption {

    enum Key: String {

        case bonusLab = "BonusLab"
        case notifications = "Notificaciones"
        case blog = "Blog"
        case myPurchases = "Mis compras"
        case labCard = "LabCard"

    }

}
