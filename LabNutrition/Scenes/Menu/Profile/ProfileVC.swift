//
//  ProfileVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ProfileVC: BaseVC {

    // MARK: - Actions
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionField(_ btn: UIButton) {
        if btn.tag == 0 {
            BottomAlertHUDView.showBottomAlert(with: "Nombres", and: self)
        }
    }

}

extension ProfileVC: BottomAlertHUDViewDelegate {

    func onOk() {}

}
