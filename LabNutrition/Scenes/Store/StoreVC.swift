//
//  StoreVC2.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class StoreVC: BaseVC {

    // MARK: - Outlets
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var tvCategories: UITableView!

    // MARK: - Views
    let btnPromotions = UIButton.create(with: .promotion)
    let btnFilter = UIButton.create(with: .filter)
    let btnSearch = UIButton.create(with: .search)

    // MARK: - Props
    weak var coordinator: StoreCoordinator!
    var viewModel: StoreVMProtocol!
    var arrStoreSection: [StoreSection] = []

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Setup
    override func setupView() {
        navigationView.title = "Tienda"
        navigationView.backButtonIsHidden = true
        navigationView.addTrailing([btnPromotions, btnFilter, btnSearch])
        btnPromotions.addTarget(self, action: #selector(actionPromotions), for: .touchUpInside)
        btnFilter.addTarget(self, action: #selector(actionFilter), for: .touchUpInside)
    }

    override func setupBinding() {
        viewModel.displaySections.bind(to: self) { (self, arrStoreSection) in
            self.arrStoreSection = arrStoreSection
            self.tvCategories.reloadData()
        }
    }

    // MARK: - Actions
    @objc func actionPromotions() {
        coordinator.showPromotionsFlow()
    }

    @objc func actionFilter() {
        coordinator.showFilterFlow()
    }

}

extension StoreVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrStoreSection.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let storeSection = arrStoreSection[indexPath.row]
        switch storeSection.type {
        case .category:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "categoryCell",
                for: indexPath) as? StoreCategorySectionCell else {
                return UITableViewCell()
            }
            cell.coordinator = coordinator
            cell.configure(with: storeSection)
            return cell
        case .promotion:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "promotionsDayCell",
                for: indexPath
                ) as? StorePromotionsSectionCell else {
                return UITableViewCell()
            }
            cell.configure(with: storeSection)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storeSection = arrStoreSection[indexPath.row]
        switch storeSection.type {
        case .promotion:
            coordinator.showProductFlow()
        default:
            break
        }
    }

}

extension StoreVC: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UITableView {
            let isOnTop = scrollView.contentOffset.y < 10
            navigationView.navigationStyle = isOnTop ? .white : .red
        }
    }

}
