//
//  StoreSectionCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class StoreSectionCell: UITableViewCell {

    // MARK: - Props
    var storeSection: StoreSection!

    // MARK: - Outlet
    @IBOutlet weak var ivSection: UIImageView!
    @IBOutlet weak var lblSection: UILabel!

    // MARK: - Setup
    func configure(with storeSection: StoreSection) {
        self.storeSection = storeSection
        ivSection.image = storeSection.image
        ivSection.tintColor = .darkGray
        ivSection.backgroundColor = .clear
        lblSection.text = storeSection.title
        lblSection.textColor = .darkGray
        lblSection.font = .labFont(with: 13)
    }

}
