//
//  StoreCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class StoreCoordinator: BaseCoordinator {

    override func start() {
        let viewController: StoreVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = StoreVM()
        push(viewController: viewController)
    }

    func showPromotionsFlow() {
        let coordinator = PromotionsCoordinator(presenter: presenter)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
    }

    func showFilterFlow() {
        let navigationController = UINavigationController()
        let coordinator = FilterCoordinator(presenter: navigationController)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
        presenter.present(navigationController, animated: true)
    }

    func showProductFlow() {
        let navigationController = UINavigationController()
        let coordinator = ProductCoordinator(presenter: navigationController)
        coordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
        }
        add(child: coordinator)
        coordinator.start()
        presenter.present(navigationController, animated: true)
    }

}
