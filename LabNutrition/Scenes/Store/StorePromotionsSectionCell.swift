//
//  StorePromotionsSectionCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class StorePromotionsSectionCell: StoreSectionCell {

    // MARK: - Outlet
    @IBOutlet weak var vPromotion: UIView!
    @IBOutlet weak var ivPromotion: UIImageView!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblPromotionPrice: UILabel!
    @IBOutlet weak var lblPreviousPrice: UILabel!

    // MARK: - Setup
    override func configure(with storeSection: StoreSection) {
        super.configure(with: storeSection)
        backgroundColor = .projectYellow
        vPromotion.cornerRadius = 5
        vPromotion.set(shadow: LabShadow())
        guard let promotion = storeSection.promotion else { return }
        ivPromotion.backgroundColor = .black
        lblProduct.font = .labFont(with: 13, and: .bold)
        lblProduct.textColor = .black
        lblProduct.text = promotion.productName
        lblBrand.font = .labFont(with: 13, and: .bold)
        lblBrand.textColor = .lightGray
        lblBrand.text = promotion.brand
        lblPromotionPrice.font = .labFont(with: 14, and: .bold)
        lblPromotionPrice.textColor = .black
        lblPromotionPrice.text = promotion.promotionPrice
        lblPreviousPrice.font = .labFont(with: 13)
        lblPreviousPrice.textColor = .lightGray
        lblPreviousPrice.setTextStrikethrough(promotion.previousPrice)
    }

}
