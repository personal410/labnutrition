//
//  StoreVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

struct StoreSection {

    let image: UIImage
    let title: String
    let type: SectionType
    var showCleanFilter: Bool = false

    var products: [ProductViewModel] {
        switch type {
        case .category(let arrProducts):
            return arrProducts
        case .promotion:
            return []
        }
    }

    var promotion: Promotion? {
        switch type {
        case .promotion(let promotion):
            return promotion
        case .category:
            return nil
        }
    }

}

extension StoreSection {

    enum SectionType {

        case category([ProductViewModel])
        case promotion(Promotion)

    }

}
