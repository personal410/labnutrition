//
//  StoreProductCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class StoreProductCell: UICollectionViewCell {

    // MARK: - Outlet
    @IBOutlet weak var productView: ProductView!

    // MARK: - Setup
    func configure(with product: ProductViewModel) {
        productView.configure(with: product)
    }

}
