//
//  StoreCategorySectionCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class StoreCategorySectionCell: StoreSectionCell {

    // MARK: - Props
    weak var coordinator: StoreCoordinator!

    // MARK: - Outlet
    @IBOutlet weak var btnCleanFilter: UIButton!
    @IBOutlet weak var cvProducts: UICollectionView!

    // MARK: - Setup
    override func configure(with storeSection: StoreSection) {
        super.configure(with: storeSection)
        btnCleanFilter.isHidden = !storeSection.showCleanFilter
        if storeSection.showCleanFilter {
            btnCleanFilter.cornerRadius = 10
            btnCleanFilter.backgroundColor = .lightGray
            btnCleanFilter.setTitle("LIMPIAR FILTROS", withFont: .labFont(with: 13), withColor: .white)
        }
    }

}

extension StoreCategorySectionCell: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        storeSection.products.count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "productCell",
            for: indexPath
            ) as? StoreProductCell else {
                return UICollectionViewCell()
        }
        cell.configure(with: storeSection.products[indexPath.item])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        coordinator.showProductFlow()
    }

}
