//
//  StoreVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol StoreVMProtocol: BaseVMProtocol {

    var displaySections: Observable<[StoreSection]> { get set }

}

class StoreVM: StoreVMProtocol {

    var displaySections = Observable<[StoreSection]>()

    func loadView() {
        var arrStoreSection: [StoreSection] = []
        arrStoreSection.append(
            StoreSection(
                image: .icArm,
                title: "CONSTRUYE MÚSCULO",
                type: .category([
                    .init(product: .productTest),
                    ProductViewModel(product: .productTest2),
                    ProductViewModel(product: .productTest)
                ]),
                showCleanFilter: true
            )
        )
        arrStoreSection.append(
            StoreSection(
                image: .icPromotions,
                title: "PROMOCIÓN DEL DÍA",
                type: .promotion(Promotion.promotionTest)
            )
        )
        arrStoreSection.append(
            StoreSection(
                image: .icLighting,
                title: "AUMENTA TU ENERGÍA",
                type: .category([
                    ProductViewModel(product: .productTest2),
                    ProductViewModel(product: .productTest)
                ]),
                showCleanFilter: false
            )
        )
        displaySections.notify(with: arrStoreSection)
    }

}
