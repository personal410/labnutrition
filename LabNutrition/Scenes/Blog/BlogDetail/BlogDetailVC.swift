//
//  BlogDetailVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class BlogDetailVC: BaseVC {

    // MARK: - Props
    var viewModel: BlogDetailVMProtocol!

    // MARK: - Outlet
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vGradient: LabView!
    @IBOutlet weak var ivNews: UIImageView!
    @IBOutlet weak var ivDate: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var stckShare: UIStackView!
    @IBOutlet weak var ivShare: UIImageView!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var vSeparator: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    // MARK: - Setup
    override func setupView() {
        btnBack.setImage(.icBack, for: .normal)
        btnBack.tintColor = .white
        ivNews.contentMode = .scaleAspectFill
        vGradient.addGradient = true
        vGradient.startColor = .black
        vGradient.endColor = .clear
        ivDate.image = .icCalendar
        ivDate.tintColor = .lightGray
        lblDate.font = .labFont(with: 13)
        lblDate.textColor = .lightGray
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionShare))
        stckShare.addGestureRecognizer(tapGesture)
        ivShare.image = .icShare
        ivShare.tintColor = .lightGray
        lblShare.font = .labFont(with: 13)
        lblShare.textColor = .lightGray
        lblShare.text = "COMPARTIR"
        vSeparator.backgroundColor = .lightGray
        lblTitle.font = .labFont(with: 20)
        lblTitle.textColor = .projectRed
        lblDescription.font = .labFont(with: 15)
        lblDescription.textColor = .black
    }

    override func setupBinding() {
        viewModel.displayNewsImage.bind(to: self) { (self, image) in
            self.ivNews.image = image
        }
        viewModel.displayNewsDate.bind(to: self) { (self, date) in
            self.lblDate.text = date
        }
        viewModel.displayNewsTitle.bind(to: self) { (self, title) in
            self.lblTitle.text = title
        }
        viewModel.displayNewsDescription.bind(to: self) { (self, description) in
            self.lblDescription.text = description
        }
    }

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    // MARK: - Action
    @IBAction func actionBack() {
        navigationController?.popViewController(animated: true)
    }

    @objc func actionShare() {
        print("share")
    }

}
