//
//  BlogDetailVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 13/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

protocol BlogDetailVMProtocol: BaseVMProtocol {

    var displayNewsImage: Observable<UIImage> { get set }
    var displayNewsDate: Observable<String> { get set }
    var displayNewsTitle: Observable<String> { get set }
    var displayNewsDescription: Observable<String> { get set }

}

class BlogDetailVM: BlogDetailVMProtocol {

    private let blogNews: BlogNewsViewModel
    var displayNewsImage = Observable<UIImage>()
    var displayNewsDate = Observable<String>()
    var displayNewsTitle = Observable<String>()
    var displayNewsDescription = Observable<String>()

    init(blogNews: BlogNewsViewModel) {
        self.blogNews = blogNews
    }

    func loadView() {
        displayNewsImage.notify(with: .bgGirl)
        displayNewsDate.notify(with: "13 JULIO 2020")
        displayNewsTitle.notify(with: blogNews.title)
        displayNewsDescription.notify(with: "Aqui va la descripción")
    }

}
