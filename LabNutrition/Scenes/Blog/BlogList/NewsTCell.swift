//
//  NewsTCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/12/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas

class NewsTCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var vContainer: UIView!
    @IBOutlet weak var ivNews: UIImageView!
    @IBOutlet weak var vGradient: LabView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivArrow: UIImageView!

    // MARK: - View lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        vContainer.set(shadow: LabShadow())
        vContainer.cornerRadius = 5
        vContainer.backgroundColor = .white
        ivNews.contentMode = .scaleAspectFill
        ivNews.cornerRadius = 5
        vGradient.addGradient = true
        vGradient.backgroundColor = .clear
        vGradient.cornerRadius = 5
        vGradient.clipsToBounds = true
        ivArrow.image = .icArrowDown
        ivArrow.tintColor = .projectRed
        ivArrow.set(rotationFactor: 1.5)
        lblTitle.font = .labFont(with: 17)
        lblTitle.textColor = .white
    }

    // MARK: - Setup
    func configure(with news: BlogNewsViewModel) {
        ivNews.image = .bgGirl
        lblTitle.text = news.title
    }

}
