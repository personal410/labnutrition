//
//  BlogNewsVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

struct BlogNewsViewModel {

    // MARK: - Props
    let blogNews: BlogNews
    var title: String { blogNews.title }

}
