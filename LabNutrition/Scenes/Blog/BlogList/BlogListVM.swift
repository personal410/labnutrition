//
//  BlogVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 13/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol BlogListVMProtocol: BaseVMProtocol {

    var displayNews: Observable<[BlogNewsViewModel]> { get set }

}

class BlogListVM: BlogListVMProtocol {

    var displayNews = Observable<[BlogNewsViewModel]>()

    func loadView() {
        let arrNews = [
            BlogNews.blogNewsTest1,
            BlogNews.blogNewsTest2,
            BlogNews.blogNewsTest1,
            BlogNews.blogNewsTest2
        ]
        displayNews.notify(with: arrNews.map { BlogNewsViewModel(blogNews: $0) })
    }

}
