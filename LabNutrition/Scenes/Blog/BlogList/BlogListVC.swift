//
//  BlogListVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class BlogListVC: BaseVC {

    // MARK: - Props
    weak var coordinator: BlogCoordinator!
    var viewModel: BlogListVMProtocol!
    var arrNews: [BlogNewsViewModel] = []

    // MARK: - Views
    let btnSearch = UIButton.create(with: .search)

    // MARK: - Outlets
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var tvBlog: UITableView!

    // MARK: - Setup
    override func setupView() {
        navigationView.title = "Blog"
        navigationView.addTrailing(btnSearch)
    }

    override func setupBinding() {
        navigationView.didBackPressed.bind(to: self) { (self, _) in
            self.coordinator.finish()
        }
        viewModel.displayNews.bind(to: self) { (self, arrNews) in
            self.arrNews = arrNews
            self.tvBlog.reloadData()
        }
    }

    // MARK: - VC Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

}

extension BlogListVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrNews.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "newsCell",
            for: indexPath) as? NewsTCell else {
                return UITableViewCell()
        }
        let news = arrNews[indexPath.row]
        cell.configure(with: news)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let news = arrNews[indexPath.row]
        coordinator.showDetail(with: news)
    }

}
