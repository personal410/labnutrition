//
//  BlogCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

final class BlogCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<BlogCoordinator>()

    override func start() {
        let viewController: BlogListVC = .instantiate()
        viewController.coordinator = self
        viewController.viewModel = BlogListVM()
        push(viewController: viewController)
    }

    func showDetail(with blogNews: BlogNewsViewModel) {
        let viewController: BlogDetailVC = .instantiate()
        viewController.hidesBottomBarWhenPushed = true
        viewController.viewModel = BlogDetailVM(blogNews: blogNews)
        push(viewController: viewController)
    }

    func finish() {
        presenter.popViewController(animated: true)
        didFinish.notify(with: self)
    }
}
