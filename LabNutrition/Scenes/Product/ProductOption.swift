//
//  ProductOption.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

enum ProductOption: Int {

    case buy
    case info
    case rates

    var viewController: UIViewController {
        switch self {
        case .buy:
            return ProductBuyVC.instantiate()
        case .info:
            return ProductInfoVC.instantiate()
        case .rates:
            return ProductRatesVC.instantiate()
        }
    }

}
