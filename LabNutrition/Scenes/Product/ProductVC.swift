//
//  ProductVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ProductVC: BaseVC {

    // MARK: - Outlet
    @IBOutlet weak var foBuy: FilterOptionView!
    @IBOutlet weak var foInfo: FilterOptionView!
    @IBOutlet weak var foRates: FilterOptionView!
    @IBOutlet weak var vContent: UIView!

    // MARK: - Props
    weak var coordinator: ProductCoordinator!
    var currentOption: Int = -1 {
        willSet {
            if currentOption > -1 {
                arrFilterOption[currentOption].setSelected(false)
            }
        }
        didSet {
            arrFilterOption[currentOption].setSelected(true)
            loadCurrentOption()
        }
    }
    var arrFilterOption: [FilterOptionView] = []
    var dicViewController: [ProductOption: UIViewController] = [:]

    // MARK: - Setup
    override func setupView() {
        arrFilterOption = [foBuy, foInfo, foRates]
        currentOption = 0
    }

    // MARK: - Actions
    @IBAction func actionBack() {
        coordinator.finish()
    }

    @IBAction func actionFilterOption(_ btn: UIButton) {
        currentOption = btn.tag
    }

    private func loadCurrentOption() {
        guard let productOption = ProductOption(rawValue: currentOption) else { return }
        for child in children {
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
        if let viewController = dicViewController[productOption] {
            vContent.addSubview(viewController.view)
            addChild(viewController)
        } else {
            let viewController = productOption.viewController
            dicViewController[productOption] = viewController
            viewController.view.frame = CGRect(origin: .zero, size: vContent.frame.size)
            vContent.addSubview(viewController.view)
            addChild(viewController)
        }
    }

}
