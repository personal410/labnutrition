//
//  ProductBuyVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ProductBuyVC: BaseVC {

    // MARK: - Props
    var arrDescriptions: [String] = [
        "Test1",
        "Test2",
        "Test3"
    ]

    // MARK: - Outlets
    @IBOutlet weak var stackDescription: UIStackView!

    // MARK: - View
    override func setupView() {
        for desc in arrDescriptions {
            let vRedCircle = UIView(frame: CGRect(x: 5, y: 5, width: 5, height: 5))
            vRedCircle.backgroundColor = .red
            vRedCircle.layer.cornerRadius = 2.5
            let vContentCircle = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            vContentCircle.addSubview(vRedCircle)
            let constLayout = NSLayoutConstraint(
                item: vContentCircle,
                attribute: .width,
                relatedBy: .equal,
                toItem: nil,
                attribute:
                .notAnAttribute,
                multiplier: 1,
                constant: 15
            )
            vContentCircle.addConstraint(constLayout)
            let lblDesc = UILabel()
            lblDesc.text = desc
            lblDesc.numberOfLines = 0
            lblDesc.font = UIFont.systemFont(ofSize: 17)
            let svOneDesc = UIStackView()
            svOneDesc.axis = .horizontal
            svOneDesc.distribution = .fill
            svOneDesc.alignment = .top
            svOneDesc.addArrangedSubview(vContentCircle)
            svOneDesc.addArrangedSubview(lblDesc)
            stackDescription.addArrangedSubview(svOneDesc)
        }
    }
}
