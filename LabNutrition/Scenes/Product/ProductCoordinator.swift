//
//  ProductCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 24/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

final class ProductCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<ProductCoordinator>()

    override func start() {
        presenter.modalPresentationStyle = .fullScreen
        presenter.navigationBar.isHidden = true
        let viewController: ProductVC = .instantiate()
        viewController.coordinator = self
        push(viewController: viewController)
    }

    func finish() {
        presenter.dismiss(animated: true, completion: nil)
        didFinish.notify(with: self)
    }

}
