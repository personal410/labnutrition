//
//  FilterCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore
import UIKit

final class FilterCoordinator: BaseCoordinator, FinishCoordinator {

    var didFinish = Observable<FilterCoordinator>()
    var objectivesVC: FilterObjectivesVC?
    var categoriesVC: FilterCategoriesVC?
    var priceVC: FilterPriceVC?
    var brandsVC: FilterBrandsVC?

    private var objectivesHandler: FilterSaveStateHandler? {
        objectivesVC?.viewModel as? FilterSaveStateHandler
    }

    private var categoriesHandler: FilterSaveStateHandler? {
        categoriesVC?.viewModel as? FilterSaveStateHandler
    }

    private var priceHandler: FilterSaveStateHandler? {
        priceVC?.viewModel as? FilterSaveStateHandler
    }

    private var brandsHandler: FilterSaveStateHandler? {
        brandsVC?.viewModel as? FilterSaveStateHandler
    }

    override func start() {
        presenter.modalPresentationStyle = .fullScreen
        presenter.navigationBar.isHidden = true
        let viewController: FilterVC = .instantiate()
        viewController.coordinator = self
        for filterOption in FilterOption.allCases {
            switch filterOption {
            case .objective:
                let childVC: FilterObjectivesVC = .instantiate()
                childVC.viewModel = FilterObjectivesVM()
                objectivesVC = childVC
                viewController.dicViewController[filterOption] = childVC
            case .category:
                let childVC: FilterCategoriesVC = .instantiate()
                childVC.viewModel = FilterCategoriesVM()
                categoriesVC = childVC
                viewController.dicViewController[filterOption] = childVC
            case .price:
                let childVC: FilterPriceVC = .instantiate()
                childVC.viewModel = FilterPriceVM()
                priceVC = childVC
                viewController.dicViewController[filterOption] = childVC
            case .brand:
                let childVC: FilterBrandsVC = .instantiate()
                childVC.viewModel = FilterBrandVM()
                brandsVC = childVC
                viewController.dicViewController[filterOption] = childVC
            }
        }
        push(viewController: viewController)
    }

    func filterAndFinish() {
        objectivesHandler?.saveState()
        categoriesHandler?.saveState()
        priceHandler?.saveState()
        brandsHandler?.saveState()
        finish()
    }

    func finish() {
        presenter.dismiss(animated: true, completion: nil)
        didFinish.notify(with: self)
    }

}
