//
//  FilterCategoriesVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class FilterCategoriesVC: BaseVC {

    // MARK: - Props
    var viewModel: FilterCategoriesVMProtocol!
    var arrCategories: [CategoryViewModel] = []

    // MARK: - Outlets
    @IBOutlet weak var tvCategoryList: UITableView!

    // MARK: - ViewCont
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

    override func setupBinding() {
        viewModel.displayCategories.bind(to: self) { (self, categories) in
            self.arrCategories = categories
            self.tvCategoryList.reloadData()
        }
        viewModel.displaySelectedIndexPaths.bind(to: self) { (self, indexPaths) in
            for indexPath in indexPaths {
                self.tvCategoryList.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }

}

extension FilterCategoriesVC: LabTableViewProtocol {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "categoryCell",
            for: indexPath) as? FilterCategoryCell else {
            return UITableViewCell()
        }
        cell.bind(with: arrCategories[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.updateSelection(with: indexPath.row, isAdded: true)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        viewModel.updateSelection(with: indexPath.row, isAdded: false)
    }

}
