//
//  FilterCategoriesVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 30/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol FilterCategoriesVMProtocol: FilterSelectionVMProtocol {

    var displayCategories: Observable<[CategoryViewModel]> { get set }

}

class FilterCategoriesVM: FilterCategoriesVMProtocol, FilterSaveStateHandler {

    private let arrCategories = LabCategory.all
    private var arrSelectedIds: [Int] = []

    var displayCategories = Observable<[CategoryViewModel]>()
    var displaySelectedIndexPaths = Observable<[IndexPath]>()

    func loadView() {
        let categories = arrCategories.map { CategoryViewModel(category: $0) }
        displayCategories.notify(with: categories)
        arrSelectedIds = FilterManager.shared.arrCategorySelected
        var arrSelectedIndex: [IndexPath] = []
        for categoryId in arrSelectedIds {
            if let index = arrCategories.firstIndex(where: { (category) -> Bool in
                category.categoryId == categoryId
            }) {
                arrSelectedIndex.append(IndexPath(row: index, section: 0))
            }
        }
        displaySelectedIndexPaths.notify(with: arrSelectedIndex)
    }

    func updateSelection(with index: Int, isAdded added: Bool) {
        if added {
            arrSelectedIds.append(arrCategories[index].categoryId)
        } else {
            if let indexToRemove = arrSelectedIds.firstIndex(of: arrCategories[index].categoryId) {
                arrSelectedIds.remove(at: indexToRemove)
            }
        }
    }

    func saveState() {
        FilterManager.shared.arrCategorySelected = arrSelectedIds
    }

}
