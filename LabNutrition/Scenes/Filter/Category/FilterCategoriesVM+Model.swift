//
//  FilterCategoriesVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 31/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

struct CategoryViewModel {

    let category: LabCategory

    var description: String { category.description }

}
