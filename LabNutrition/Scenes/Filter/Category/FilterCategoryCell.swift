//
//  FilterCategoryCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class FilterCategoryCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivSelected: UIImageView!

    // MARK: - Setup
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.font = .labFont(with: 13)
        lblName.textColor = .lightGray
        ivSelected.image = .icSwitch
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivSelected.tintColor = isSelected ? .projectBlue : .lightGray
        ivSelected.transform = isSelected ? .identity : .init(rotationAngle: CGFloat(Double.pi))
    }

    func bind(with category: CategoryViewModel) {
        lblName.text = category.description
    }

}
