//
//  FilterBrandCell.swift
//  LabNutrition
//
//  Created by Victor Salazar on 12/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class FilterBrandCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var ivSelected: UIImageView!

    // MARK: - Auxiliar
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ivSelected.tintColor = isSelected ? .projectBlue : .lightGray
        ivSelected.transform = isSelected ? .identity : .init(rotationAngle: CGFloat.pi)
    }

    func bind(with brand: BrandViewModel) {
        lblName.text = brand.description
    }

}
