//
//  FilterBrandVM+Model.swift
//  LabNutrition
//
//  Created by Victor Salazar on 7/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

struct BrandViewModel {

    let brand: Brand

    var description: String { brand.description }

}
