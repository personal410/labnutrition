//
//  FilterBrandVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 7/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol FilterBrandVMProtocol: FilterSelectionVMProtocol {

    var displayBrandList: Observable<[BrandViewModel]> { get set }

}

class FilterBrandVM: FilterBrandVMProtocol, FilterSaveStateHandler {

    private let arrBrands = Brand.all
    private var arrSelectedIds: [Int] = []

    var displayBrandList = Observable<[BrandViewModel]>()
    var displaySelectedIndexPaths = Observable<[IndexPath]>()

    func loadView() {
        let brands = arrBrands.map { BrandViewModel(brand: $0) }
        displayBrandList.notify(with: brands)
        arrSelectedIds = FilterManager.shared.arrBrandSelected
        var arrSelectedIndex: [IndexPath] = []
        for brandId in arrSelectedIds {
            if let index = arrBrands.firstIndex(where: { (brand) -> Bool in
                brand.brandId == brandId
            }) {
                arrSelectedIndex.append(IndexPath(row: index, section: 0))
            }
        }
        displaySelectedIndexPaths.notify(with: arrSelectedIndex)
    }

    func updateSelection(with index: Int, isAdded added: Bool) {
        if added {
            arrSelectedIds.append(arrBrands[index].brandId)
        } else {
            if let indexToRemove = arrSelectedIds.firstIndex(of: arrBrands[index].brandId) {
                arrSelectedIds.remove(at: indexToRemove)
            }
        }
    }

    func saveState() {
        FilterManager.shared.arrBrandSelected = arrSelectedIds
    }

}
