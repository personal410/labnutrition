//
//  FilterObjectivesVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 26/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol FilterObjectivesVMProtocol: FilterSelectionVMProtocol {

    var displayObjectiveList: Observable<[ObjectiveViewModel]> { get set }

}

class FilterObjectivesVM: FilterObjectivesVMProtocol, FilterSaveStateHandler {

    private let arrObjective = Objective.all
    private var arrSelectedIds: [Int] = []

    var displayObjectiveList = Observable<[ObjectiveViewModel]>()
    var displaySelectedIndexPaths = Observable<[IndexPath]>()

    func loadView() {
        let arrObjectiveModel = arrObjective.map { ObjectiveViewModel(objective: $0) }
        displayObjectiveList.notify(with: arrObjectiveModel)
        arrSelectedIds = FilterManager.shared.arrObjectiveSelected
        var arrSelectedIndex: [IndexPath] = []
        for objectiveId in arrSelectedIds {
            if let index = arrObjective.firstIndex(where: { (objective) -> Bool in
                objective.objectiveId == objectiveId
            }) {
                arrSelectedIndex.append(IndexPath(row: index, section: 0))
            }
        }
        displaySelectedIndexPaths.notify(with: arrSelectedIndex)
    }

    func updateSelection(with index: Int, isAdded added: Bool) {
        if added {
            arrSelectedIds.append(arrObjective[index].objectiveId)
        } else {
            if let indexToRemove = arrSelectedIds.firstIndex(of: arrObjective[index].objectiveId) {
                arrSelectedIds.remove(at: indexToRemove)
            }
        }
    }

    func saveState() {
        FilterManager.shared.arrObjectiveSelected = arrSelectedIds
    }

}
