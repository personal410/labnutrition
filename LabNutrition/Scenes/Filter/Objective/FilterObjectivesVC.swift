//
//  FilterObjectivesVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class FilterObjectivesVC: BaseVC {

    // MARK: - Props
    var viewModel: FilterObjectivesVMProtocol!
    var arrObjectives: [ObjectiveViewModel] = []

    // MARK: - Outlets
    @IBOutlet weak var tvObjectiveList: UITableView!

    // MARK: - Setup
    override func setupBinding() {
        viewModel.displayObjectiveList.bind(to: self) { (self, objectiveViewModels) in
            self.arrObjectives = objectiveViewModels
            self.tvObjectiveList.reloadData()
        }
        viewModel.displaySelectedIndexPaths.bind(to: self) { (self, indexPaths) in
            for indexPath in indexPaths {
                self.tvObjectiveList.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

}

extension FilterObjectivesVC: LabTableViewProtocol {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrObjectives.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "objectiveCell",
            for: indexPath) as? ObjectiveCell else {
            return UITableViewCell()
        }
        cell.style = .lightGray
        cell.configure(with: arrObjectives[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.updateSelection(with: indexPath.row, isAdded: true)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        viewModel.updateSelection(with: indexPath.row, isAdded: false)
    }

}
