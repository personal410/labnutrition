//
//  Filter+Protocol.swift
//  LabNutrition
//
//  Created by Victor Salazar on 12/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol FilterSelectionVMProtocol: BaseVMProtocol {

    var displaySelectedIndexPaths: Observable<[IndexPath]> { get set }

    func updateSelection(with index: Int, isAdded added: Bool)

}

protocol FilterSaveStateHandler {

    func saveState()

}
