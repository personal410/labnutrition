//
//  FilterVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class FilterVC: BaseVC {

    // MARK: - Outlets
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var foObjectives: FilterOptionView!
    @IBOutlet weak var foCategories: FilterOptionView!
    @IBOutlet weak var foPrice: FilterOptionView!
    @IBOutlet weak var foBrands: FilterOptionView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var btnFilter: UIButton!

    // MARK: - Views
    let btnClose = UIButton.create(with: .close)

    // MARK: - Props
    var arrFilterOptionView: [FilterOptionView] = []
    var currentOption: Int = -1 {
        willSet {
            if currentOption > -1 {
                arrFilterOptionView[currentOption].setSelected(false)
            }
        }
        didSet {
            arrFilterOptionView[currentOption].setSelected(true)
            loadCurrentOption()
        }
    }
    weak var coordinator: FilterCoordinator!
    var dicViewController: [FilterOption: UIViewController] = [:]

    override func setupView() {
        navigationView.backButtonIsHidden = true
        navigationView.title = "Filtros"
        navigationView.addTrailing(btnClose)
        btnClose.addTarget(self, action: #selector(actionClose), for: .touchUpInside)
        arrFilterOptionView = [foObjectives, foCategories, foPrice, foBrands]
        foObjectives.text = "Objetivos"
        foCategories.text = "Categorías"
        foPrice.text = "Precio"
        foBrands.text = "Marcas"
        currentOption = 0
        btnFilter.setTitle("Filtrar", withFont: .labFont(with: 17), withColor: .white)
        btnFilter.backgroundColor = .projectRed
    }

    @objc func actionClose() {
        coordinator.finish()
    }

    @IBAction func actionFilterOption(_ btn: UIButton) {
        currentOption = btn.tag
    }

    @IBAction func actionFilter() {
        coordinator.filterAndFinish()
    }

    private func loadCurrentOption() {
        guard let filterOption = FilterOption(rawValue: currentOption) else { return }
        for child in children {
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
        guard let viewController = dicViewController[filterOption] else { return }
        viewController.view.frame.size = viewContent.frame.size
        viewContent.addSubview(viewController.view)
        addChild(viewController)
    }

}
