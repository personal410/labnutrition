//
//  FilterOption.swift
//  LabNutrition
//
//  Created by Victor Salazar on 23/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

enum FilterOption: Int, CaseIterable {

    case objective
    case category
    case price
    case brand

}
