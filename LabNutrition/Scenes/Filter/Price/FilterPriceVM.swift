//
//  FilterPriceVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 3/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabCore

protocol FilterPriceVMProtocol: BaseVMProtocol {

    var displayMaxPrice: Observable<Int> { get set }

    func updateMaxPrice(with maxPrice: Int)

}

class FilterPriceVM: FilterPriceVMProtocol, FilterSaveStateHandler {

    private var maxPrice = FilterManager.shared.maximumPrice
    var displayMaxPrice = Observable<Int>()

    func loadView() {
        displayMaxPrice.notify(with: maxPrice)
    }

    func updateMaxPrice(with maxPrice: Int) {
        self.maxPrice = maxPrice
    }

    func saveState() {
        FilterManager.shared.maximumPrice = maxPrice
    }

}
