//
//  FilterPriceVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class FilterPriceVC: BaseVC {

    // MARK: - Props
    var viewModel: FilterPriceVMProtocol!

    // MARK: - Outlet
    @IBOutlet weak var lblUntil: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var circularSliderMaxPrice: CircularSliderView!

    override func setupView() {
        lblUntil.text = "HASTA"
        lblUntil.font = .labFont(with: 18)
        lblUntil.textColor = .projectRed
        lblPrice.font = .labFont(with: 30)
        lblPrice.textColor = .projectRed
    }

    override func setupBinding() {
        viewModel.displayMaxPrice.bind(to: self) { (self, maxPrice) in
            self.circularSliderMaxPrice.value = maxPrice
            self.lblPrice.text = "S/ \(self.circularSliderMaxPrice.value).00"
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadView()
    }

}

extension FilterPriceVC: CircularSliderViewDelegate {

    func circularSliderDidChange(_ circularSliderView: CircularSliderView, to value: Int) {
        lblPrice.text = "S/ \(value).00"
        viewModel.updateMaxPrice(with: value)
    }

}
