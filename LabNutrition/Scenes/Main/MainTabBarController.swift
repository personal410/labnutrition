//
//  MainTabBarController.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    // MARK: - Props
    weak var coordinator: MainCoordinator!

    // MARK: - VC Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        coordinator.showOnboardingIfNeeded()
    }

}
