//
//  MainOption.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

struct MainOption {

    static let home = MainOption(key: .home, image: .icHome, index: 0)
    static let store = MainOption(key: .store, image: .icStore, index: 1)
    static let benefits = MainOption(key: .benefits, image: .icBenefits, index: 2)
    static let shoppingCart = MainOption(key: .shoppingCart, image: .icShoppingCart, index: 3)
    static let more = MainOption(key: .more, image: .icMore, index: 4)

    let key: MainOption.Key
    let image: UIImage
    let index: Int

    static let all: [MainOption] = [.home, .store, .benefits, .shoppingCart, .more]

}

extension MainOption {

    enum Key {

        case home
        case store
        case benefits
        case shoppingCart
        case more

    }

}
