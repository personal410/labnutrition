//
//  MainCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore
import LabCanvas

final class MainCoordinator: BaseCoordinator, FinishCoordinator {

    // MARK: - Props
    var didFinish = Observable<MainCoordinator>()
    private var tabBarController = MainTabBarController()

    // MARK: - Cycle
    override func start() {
        tabBarController.coordinator = self
        tabBarController.tabBar.tintColor = .projectRed
        tabBarController.tabBar.isTranslucent = false
        for mainOption in MainOption.all {
            let coordinator = create(from: mainOption)
            add(child: coordinator)
            coordinator.start()
        }
        push(viewController: tabBarController)
    }

    func showOnboardingIfNeeded() {
        if Session.shared.isNewUser {
            let navigationController = UINavigationController()
            let coordinator = CustomizeExperienceCoordinator(presenter: navigationController, isFirstLogIn: true)
            coordinator.didFinish.bind(to: self) { (self, coordinator) in
                Session.shared.isNewUser = false
                self.remove(child: coordinator)
            }
            tabBarController.present(navigationController, animated: true)
            add(child: coordinator)
            coordinator.start()
        }
    }

    func finish() {
        presenter.dismiss(animated: true, completion: nil)
        didFinish.notify(with: self)
    }

    // MARK: - Auxiliar
    func changeTab(with mainOption: MainOption) {
        tabBarController.selectedIndex = mainOption.index
    }

    private func create(from mainOption: MainOption) -> Coordinator {
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        navigationController.tabBarItem.image = mainOption.image
        navigationController.tabBarItem.title = ""
        tabBarController.add(tab: navigationController)
        switch mainOption.key {
        case .home:
            return HomeCoordinator(presenter: navigationController)
        case .store:
            return StoreCoordinator(presenter: navigationController)
        case .benefits:
            return BenefitsCoordinator(presenter: navigationController)
        case .shoppingCart:
            return ShoppingCartCoordinator(presenter: navigationController)
        case .more:
            return MenuCoordinator(presenter: navigationController)
        }
    }

}
