//
//  AppCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore

final class AppCoordinator: Coordinator {

    var parent: Coordinator?
    var children: [Coordinator] = []
    private weak var window: UIWindow!

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        if Session.shared.isUser {
            mainFlow()
        } else {
            identificationFlow()
        }
    }

    func identificationFlow() {
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        let identificationCoordinator = IdentificationCoordinator(presenter: navigationController)
        identificationCoordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
            self.mainFlow()
        }
        add(child: identificationCoordinator)
        window.rootViewController = navigationController
        identificationCoordinator.start()
    }

    func mainFlow() {
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        let mainCoordinator = MainCoordinator(presenter: navigationController)
        mainCoordinator.didFinish.bind(to: self) { (self, coordinator) in
            self.remove(child: coordinator)
            self.identificationFlow()
        }
        self.add(child: mainCoordinator)
        self.window.rootViewController = navigationController
        mainCoordinator.start()
    }

}
