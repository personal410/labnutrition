//
//  Toolbox.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class Toolbox {

    class func showAlertWithTitle(
        _ title: String,
        withMessage message: String,
        withOkButtonTitle okButtonTitle: String = "OK",
        withOkHandler handler:((_ alertAction: UIAlertAction) -> Void)? = nil,
        inViewCont viewCont: UIViewController) {
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: okButtonTitle, style: .default, handler: handler))
        viewCont.present(alertCont, animated: true, completion: nil)
    }

    class func validate(email: String) -> Bool {
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return emailTest.evaluate(with: email)
    }

    class func changePlaceholder(with color: UIColor, to textField: UITextField) {
        guard let placeholder = textField.placeholder else {
            return
        }
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.foregroundColor: color])
    }

}
