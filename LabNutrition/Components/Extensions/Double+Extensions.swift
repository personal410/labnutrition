//
//  Double+Extensions.swift
//  LabNutrition
//
//  Created by Victor Salazar on 29/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

extension Double {

    var textTwoDecimals: String { String(format: "%.2f", self) }

}
