//
//  LabTableView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

protocol LabTableViewProtocol: UITableViewDataSource, UITableViewDelegate {}
