//
//  LabImageView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 21/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

@IBDesignable class LabImageView: UIImageView {

    @IBInspectable var rotationFactor: CGFloat = 0 {
        didSet {
            if rotationFactor == 0 {
                transform = .identity
            } else {
                transform = .init(rotationAngle: CGFloat.pi * rotationFactor)
            }
        }
    }

    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 1
        }
    }

}
