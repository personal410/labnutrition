//
//  RateView.swift
//  RateView
//
//  Created by Victor Salazar on 15/10/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//

import UIKit

@IBDesignable class RateView: UIView {

    @IBInspectable var rate: CGFloat = 5.0 {
        didSet {
            if arrStarView.count > 0 {
                for index in 0 ... 4 {
                    let starView = arrStarView[index]
                    let starRate = rate - CGFloat(index)
                    starView.value = starRate > 1 ? 1 : starRate
                    starView.setNeedsDisplay()
                }
            }
        }
    }

    @IBInspectable var spaceBetweenStar: CGFloat = 5

    @IBInspectable var starColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable var borderColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }

    var arrStarView: [StarView] = []

    override func draw(_ rect: CGRect) {
        arrStarView.removeAll()
        let size = self.frame.height
        for index in 0 ... 4 {
            let posX = CGFloat(index) * (size + spaceBetweenStar)
            let star = StarView(frame: CGRect(x: posX, y: 0, width: size, height: size))
            star.fillColor = starColor
            star.borderColor = borderColor
            let starRate = rate - CGFloat(index)
            star.value = starRate > 1 ? 1 : starRate
            arrStarView.append(star)
            self.addSubview(star)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
