//
//  NavigationView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCanvas
import LabCore

enum NavigationViewStyle {

    case white
    case red

    var viewColor: UIColor {
        switch self {
        case .white:
            return .white
        case .red:
            return .projectRed
        }
    }

    var tintColor: UIColor {
        switch self {
        case .white:
            return .projectRed
        case .red:
            return .white
        }
    }

}

@IBDesignable
class NavigationView: UIView {

    // MARK: - Props
    var didBackPressed = Observable<NavigationView>()
    var navigationStyle: NavigationViewStyle = .white {
        didSet {
            backgroundColor = navigationStyle.viewColor
            btnBack.tintColor = navigationStyle.tintColor
            lblTitle.textColor = navigationStyle.tintColor
            ivLab.tintColor = navigationStyle.tintColor
            for button in arrTrailingButtons {
                button.tintColor = navigationStyle.tintColor
            }
        }
    }
    var arrTrailingButtons: [UIButton] = []
    var backButtonIsHidden: Bool {
        get { btnBack.isHidden }
        set { btnBack.isHidden = newValue }
    }
    var useImage: Bool {
        get { false }
        set {
            ivLab.isHidden = !newValue
            lblTitle.isHidden = newValue
        }
    }
    var title: String? {
        get { lblTitle.text }
        set { lblTitle.text = newValue }
    }
    override var intrinsicContentSize: CGSize {
        CGSize(width: UIScreen.main.bounds.width, height: 60)
    }

    // MARK: - Views
    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 0
        stackView.alignment = .center
        return stackView
    }()
    var btnBack: UIButton = {
        let button = UIButton()
        button.setImage(.icBack, for: .normal)
        button.tintColor = .projectRed
        return button
    }()
    var lblTitle: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 23)
        label.textColor = .projectRed
        return label
    }()
    var ivLab: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .imgLab
        imageView.tintColor = .projectRed
        return imageView
    }()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeView()
    }

    // MARK: - Auxiliar
    private func initializeView() {
        btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
        let vBtnBack = UIView()
        vBtnBack.addSubview(btnBack)
        stackView.addArrangedSubview(vBtnBack)
        stackView.addArrangedSubview(lblTitle)
        stackView.addArrangedSubview(ivLab)
        stackView.addArrangedSubview(UIView())
        addSubview(stackView)
        translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        vBtnBack.translatesAutoresizingMaskIntoConstraints = false
        btnBack.translatesAutoresizingMaskIntoConstraints = false
        ivLab.translatesAutoresizingMaskIntoConstraints = false
        vBtnBack.setupInsideView(btnBack)
        self.sameConstraint(attribute: .leading, to: stackView, with: -5)
        self.sameConstraint(attribute: .trailing, to: stackView, with: 20)
        self.sameConstraint(attribute: .centerY, to: stackView)
        btnBack.selfConstraint(attribute: .width, with: 25)
        btnBack.aspectRatio()
        ivLab.selfConstraint(attribute: .height, with: 40)
        ivLab.aspectRatio(with: 18.0 / 7.0)
        useImage = false
    }

    @objc func actionBack() {
        didBackPressed.notify(with: self)
    }

    func addTrailing(_ button: UIButton) {
        button.selfConstraint(attribute: .width, with: 36)
        button.aspectRatio()
        button.tintColor = navigationStyle.tintColor
        stackView.addArrangedSubview(button)
        arrTrailingButtons.append(button)
    }

    func addTrailing(_ arrButton: [UIButton]) {
        for button in arrButton {
            addTrailing(button)
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        invalidateIntrinsicContentSize()
    }

}
