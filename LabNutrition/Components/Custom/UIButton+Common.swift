//
//  UIButton+Common.swift
//  LabNutrition
//
//  Created by Victor Salazar on 14/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

enum LabButtonType {

    case promotion
    case search
    case filter
    case close

    var image: UIImage {
        switch self {
        case .promotion:
            return .icPromotions
        case .search:
            return .icSearch
        case .filter:
            return .icFilter
        case .close:
            return UIImage.icClose.withTintColor(.lightGray)
        }
    }

}

extension UIButton {

    static func create(with labButtonType: LabButtonType) -> UIButton {
        UIButton(image: labButtonType.image)
    }

}
