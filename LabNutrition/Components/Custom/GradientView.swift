//
//  GradientView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 28/12/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {

    @IBInspectable var topColor: UIColor = .clear
    @IBInspectable var bottomColor: UIColor = .clear

    override func draw(_ rect: CGRect) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.frame = rect
        layer.addSublayer(gradientLayer)
    }

}
