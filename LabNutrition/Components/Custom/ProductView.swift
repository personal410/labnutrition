//
//  ProductView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit
import LabCore
import LabCanvas

class ProductView: UIView {

    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 20
        stackView.alignment = .center
        return stackView
    }()

    var ivProduct = UIImageView()

    var stckInfo: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 2
        return stackView
    }()

    var lblProductName: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 15, and: .bold)
        label.textColor = .black
        return label
    }()

    var lblBrandName: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 13, and: .bold)
        label.textColor = .lightGray
        return label
    }()

    var rateView: RateView = {
        let rateView = RateView()
        rateView.starColor = .projectRed
        rateView.borderColor = .projectRed
        return rateView
    }()

    let vSpace = UIView()

    var stckAccumulatePoints: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 4
        return stackView
    }()

    var lblAccumulate: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 12)
        label.textColor = .lightGray
        label.text = "acumula"
        return label
    }()

    var lblPoints: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 12)
        label.textColor = .projectRed
        return label
    }()

    var stckPrices: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 4
        return stackView
    }()

    var lblPromotionPrice: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 14, and: .bold)
        label.textColor = .black
        return label
    }()

    var lblPreviousPrice: UILabel = {
        let label = UILabel()
        label.font = .labFont(with: 12)
        label.textColor = .lightGray
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeView()
    }

    private func initializeView() {
        cornerRadius = 5
        set(shadow: LabShadow())
        backgroundColor = .white
        stckAccumulatePoints.addArrangedSubview(lblAccumulate)
        stckAccumulatePoints.addArrangedSubview(lblPoints)
        stckPrices.addArrangedSubview(lblPromotionPrice)
        stckPrices.addArrangedSubview(lblPreviousPrice)
        stckInfo.addArrangedSubview(lblProductName)
        stckInfo.addArrangedSubview(lblBrandName)
        stckInfo.addArrangedSubview(rateView)
        stckInfo.addArrangedSubview(vSpace)
        stckInfo.addArrangedSubview(stckAccumulatePoints)
        stckInfo.addArrangedSubview(stckPrices)
        stackView.addArrangedSubview(ivProduct)
        stackView.addArrangedSubview(stckInfo)
        addSubview(stackView)
        setupConstraints()
    }

    private func setupConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        ivProduct.translatesAutoresizingMaskIntoConstraints = false
        vSpace.translatesAutoresizingMaskIntoConstraints = false
        lblAccumulate.translatesAutoresizingMaskIntoConstraints = false
        lblPromotionPrice.translatesAutoresizingMaskIntoConstraints = false
        setupInsideView(stackView, with: 15)
        ivProduct.selfConstraint(attribute: .width, with: 90)
        ivProduct.aspectRatio()
        rateView.selfConstraint(attribute: .height, with: 10)
        vSpace.selfConstraint(attribute: .height, with: 10)
        lblAccumulate.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        lblPromotionPrice.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }

    func configure(with product: ProductViewModel) {
        ivProduct.backgroundColor = .black
        lblProductName.text = product.descripcion
        lblBrandName.text = product.marca
        rateView.rate = CGFloat(Double((arc4random() % 25)) / 5.0)
        lblPoints.text = product.puntos
        lblPromotionPrice.text = product.precioPromocion.formattedString
        lblPreviousPrice.setTextStrikethrough( product.precioUnitario.formattedString)
    }

}
