//
//  FilterOptionView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

@IBDesignable class FilterOptionView: UIView {

    // MARK: - Props
    var isSelected = false
    var text: String? {
        get { lblTitle.text }
        set { lblTitle.text = newValue }
    }

    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vCircle: UIView!
    @IBInspectable var selectedColor: UIColor = .projectRed
    @IBInspectable var unselectedTitleColor: UIColor = .lightGray

    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = .labFont(with: 17)
        vCircle.layer.cornerRadius = 5
    }

    // MARK: - Auxiliar
    func setSelected(_ selected: Bool) {
        isSelected = selected
        lblTitle.textColor = isSelected ? selectedColor : unselectedTitleColor
        vCircle.backgroundColor = isSelected ? selectedColor : .clear
    }

}
