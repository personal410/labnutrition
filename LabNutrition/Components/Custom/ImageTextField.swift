//
//  ImageTextField.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/8/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//
import UIKit

@objc protocol ImageTextFieldDelegate {
    func onActionRight(_ imgTxtFld: ImageTextField)
}

@IBDesignable class ImageTextField: UITextField {

    // MARK: - IBOutlet
    @IBOutlet weak var imgTxtFldDelegate: ImageTextFieldDelegate?

    // MARK: - Props
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                if let btn = rightView as? UIButton {
                    btn.setImage(image, for: .normal)
                } else {
                    let btnRight = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    btnRight.setImage(image, for: .normal)
                    btnRight.addTarget(self, action: #selector(actionRight), for: .touchUpInside)
                    rightView = btnRight
                }
            }
        }
    }
    @IBInspectable var leftImage: UIImage? {
        didSet {
            if let image = leftImage {
                leftViewMode = .always
                let btnLeft = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                btnLeft.setImage(image, for: .normal)
                leftView = btnLeft
            }
        }
    }
    @IBInspectable var lineColor: UIColor = .clear
    @IBInspectable var imgRightTintColor: UIColor = .clear {
        didSet {
            if let btn = rightView as? UIButton {
                btn.tintColor = imgRightTintColor
            }
        }
    }
    @IBInspectable var imgLeftTintColor: UIColor = .clear {
        didSet {
            if let btn = leftView as? UIButton {
                btn.tintColor = imgLeftTintColor
            }
        }
    }

    // MARK: - Action
    @IBAction func actionRight() {
        imgTxtFldDelegate?.onActionRight(self)
    }

    // MARK: - Render
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(1)
        context.setStrokeColor(lineColor.cgColor)
        context.move(to: CGPoint(x: 0, y: rect.height - 1))
        context.addLine(to: CGPoint(x: rect.width, y: rect.height - 1))
        context.strokePath()
    }

    func set(placeholder: String, withColor color: UIColor) {
        attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [.foregroundColor: color]
        )
    }

}
