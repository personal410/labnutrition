//
//  ShadowView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 8/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

@IBDesignable class ShadowView: UIView {

    @IBInspectable var shadowColor: UIColor = .clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }

}
