//
//  CircularSliderView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 10/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

@objc protocol CircularSliderViewDelegate {

    func circularSliderDidChange(_ circularSliderView: CircularSliderView, to value: Int)

}

@IBDesignable class CircularSliderView: UIView {

    // MARK: - Props
    private var minValue: Int = 0
    private var maxValue: Int = 500
    private var step: Int = 50

    // MARK: - Views
    private var sliderView = SliderView()

    // MARK: - Outlet
    @IBOutlet weak var delegate: CircularSliderViewDelegate?

    var value: Int = 0 {
        didSet {
            if value > maxValue {
                value = maxValue
            }
            let currentAngle = 2 * CGFloat.pi * CGFloat(value) / CGFloat(maxValue) + CGFloat.pi / 2
            sliderView.currentAngle = currentAngle
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeView()
    }

    private func initializeView() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(gesturePan(_:)))
        addGestureRecognizer(panGesture)
        let backgroundView = BackgroundView()
        backgroundView.backgroundColor = .clear
        sliderView.backgroundColor = .clear
        addSubview(backgroundView)
        addSubview(sliderView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        sliderView.translatesAutoresizingMaskIntoConstraints = false
        setupInsideView(backgroundView)
        setupInsideView(sliderView)
    }

    // MARK: - Action
    @IBAction func gesturePan(_ gesture: UIPanGestureRecognizer) {
        if gesture.state == .began {
            let point = gesture.location(in: self)
            let dist = calculateDistance(between: sliderView.thumbCenter, and: point)
            if dist > 20 {
                gesture.state = .cancelled
            }
        } else if gesture.state == .changed {
            let point = gesture.location(in: self)
            let semiMinSide = sliderView.semiMinSide
            let center = sliderView.center
            let dist = calculateDistance(between: center, and: point)
            if dist < semiMinSide - 26 || dist > semiMinSide + 4 {
                gesture.state = .cancelled
                return
            }
            let atan = atan2(point.x - center.x, point.y - center.y)
            let currentAngle: CGFloat
            if atan < 0 {
                currentAngle = 0.5 * CGFloat.pi - atan
            } else {
                currentAngle = 2.5 * CGFloat.pi - atan
            }
            let newAngle = currentAngle - 0.5 * CGFloat.pi
            let newValue = Int(round(CGFloat(maxValue) * newAngle / ( 2 * CGFloat.pi * CGFloat(step)))) * step
            if abs(newValue - value) > step {
                gesture.state = .cancelled
                return
            }
            value = newValue
            sliderView.currentAngle = currentAngle
            delegate?.circularSliderDidChange(self, to: value)
        } else if gesture.state == .ended || gesture.state == .cancelled {
            let newAngle = sliderView.currentAngle - 0.5 * CGFloat.pi
            let roundedValue = Int(round(CGFloat(maxValue) * newAngle / ( 2 * CGFloat.pi * CGFloat(step)))) * step
            delegate?.circularSliderDidChange(self, to: roundedValue)
            let currentAngle = 2 * CGFloat.pi * CGFloat(roundedValue) / CGFloat(maxValue) + CGFloat.pi / 2
            sliderView.currentAngle = currentAngle
        }
    }

    // MARK: - Auxiliar
    private func calculateDistance(between firstPoint: CGPoint, and secondPoint: CGPoint) -> CGFloat {
        return sqrt(pow(firstPoint.x - secondPoint.x, 2) + pow(firstPoint.y - secondPoint.y, 2))
    }

}

fileprivate extension CircularSliderView {

    class BackgroundView: UIView {

        override func draw(_ rect: CGRect) {
            super.draw(rect)
            let width = rect.width
            let height = rect.height
            let center = CGPoint(x: width / 2.0, y: height / 2.0)
            let semiMinSide = min(width, height) / 2
            let pathInnerCircle = UIBezierPath(
                arcCenter: center,
                radius: semiMinSide - 24,
                startAngle: 0,
                endAngle: 2 * CGFloat.pi,
                clockwise: false
            )
            let pathOuterCircle = UIBezierPath(
                arcCenter: center,
                radius: semiMinSide - 8,
                startAngle: 0,
                endAngle: 2 * CGFloat.pi,
                clockwise: false
            )
            let pathCircles = UIBezierPath()
            pathCircles.append(pathInnerCircle)
            pathCircles.append(pathOuterCircle)
            let shapeBackground = CAShapeLayer()
            shapeBackground.path = pathCircles.cgPath
            shapeBackground.lineWidth = 6
            shapeBackground.strokeColor = UIColor.projectPink.cgColor
            shapeBackground.fillColor = UIColor.clear.cgColor
            layer.addSublayer(shapeBackground)
        }

    }

    class SliderView: UIView {

        var currentAngle: CGFloat = 0.5 * CGFloat.pi {
            didSet {
                setNeedsDisplay()
            }
        }
        var thumbCenter: CGPoint = .zero
        var semiMinSide: CGFloat = 0

        override func draw(_ rect: CGRect) {
            super.draw(rect)
            let width = rect.width
            let height = rect.height
            let semiWidth = width / 2.0
            let semiHeight = height / 2.0
            let context = UIGraphicsGetCurrentContext()!
            context.clear(rect)
            if semiMinSide == 0 {
                let minSide = min(width, height)
                semiMinSide = minSide / 2.0
            }
            context.setStrokeColor(UIColor.projectRed.cgColor)
            context.setLineWidth(10)
            let radius = semiMinSide - 16
            context.addArc(
                center: CGPoint(x: semiWidth, y: semiHeight),
                radius: radius,
                startAngle: CGFloat.pi / 2.0,
                endAngle: currentAngle,
                clockwise: false
            )
            context.strokePath()
            context.beginPath()
            context.setFillColor(UIColor.projectRed.cgColor)
            let tempAngle = currentAngle - CGFloat.pi / 2.0
            let posY = cos(tempAngle) * radius
            let posX = sin(tempAngle) * radius
            thumbCenter = CGPoint(x: semiWidth - posX, y: semiHeight + posY)
            context.addArc(
                center: thumbCenter,
                radius: 16,
                startAngle: 0,
                endAngle: 2 * CGFloat.pi,
                clockwise: false
            )
            context.fillPath()
        }

    }

}
