//
//  ProgressHUDView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 1/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class ProgressHUDView: UIView {

    static var progressHUDView: ProgressHUDView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .init(sameValue: 0, with: 0.7)
        let internalFrame = CGRect(
            x: frame.width / 2 - 50,
            y: frame.height / 2 - 50,
            width: 100,
            height: 100
        )
        let internalView = UIView(frame: internalFrame)
        internalView.backgroundColor = .white
        internalView.cornerRadius = 10
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.style = .large
        activityIndicatorView.color = .black
        activityIndicatorView.center = CGPoint(x: 50, y: 38.5)
        internalView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        let lbl = UILabel(frame: CGRect(x: 0, y: 62, width: 100, height: 18))
        lbl.font = .labFont(with: 15)
        lbl.textAlignment = .center
        lbl.text = "Cargando"
        internalView.addSubview(lbl)
        addSubview(internalView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    class func showProgress() {
        if progressHUDView == nil {
            let window = UIApplication.shared.windows.first!
            let progressHUDView = ProgressHUDView(frame: window.frame)
            window.addSubview(progressHUDView)
            self.progressHUDView = progressHUDView
        }
    }

    class func dismiss() {
        progressHUDView?.removeFromSuperview()
        progressHUDView = nil
    }

}
