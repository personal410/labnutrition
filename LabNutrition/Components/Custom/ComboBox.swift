//
//  ComboBox.swift
//  LabNutrition
//
//  Created by Victor Salazar on 4/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//
import UIKit

@objc protocol ComboBoxDelegate {

    func comboBox(_ comboBox: ComboBox, didSelectAt index: Int)

}

@IBDesignable class ComboBox: ImageTextField, UIPickerViewDataSource, UIPickerViewDelegate, ImageTextFieldDelegate {

    // MARK: - Props
    var arrValues: [String] = []
    var currentIndex = 0

    // MARK: - Outlet
    @IBOutlet weak var comboBoxDelegate: ComboBoxDelegate?
    @IBInspectable var defaultValue: String = "Seleccione"

    // MARK: - View
    override func didMoveToSuperview() {
        imgTxtFldDelegate = self
        let pickerView = UIPickerView(frame: .zero)
        pickerView.dataSource = self
        pickerView.delegate = self
        inputView = pickerView
        let toolbar = UIToolbar()
        let spaceBtn = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let okBtn = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(ok))
        toolbar.items = [spaceBtn, okBtn]
        toolbar.sizeToFit()
        inputAccessoryView = toolbar
        text = defaultValue
    }

    // MARK: - Auxiliar
    @objc func ok() {
        resignFirstResponder()
        if let pickerView = inputView as? UIPickerView {
            currentIndex = pickerView.selectedRow(inComponent: 0) - 1
            comboBoxDelegate?.comboBox(self, didSelectAt: currentIndex)
            if currentIndex > -1 {
                text = arrValues[currentIndex]
            } else {
                text = defaultValue
            }
        }
    }

    func setValues(_ values: [String]) {
        arrValues = values
    }

    func setSelectedIndex(_ index: Int) {
        currentIndex = index
        if index > -1 {
            text = arrValues[currentIndex]
        }
        if let pickerView = inputView as? UIPickerView {
            pickerView.selectRow(currentIndex + 1, inComponent: 0, animated: false)
        }
    }

    // MARK: - PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrValues.count + 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return row == 0 ? defaultValue : arrValues[row - 1]
    }

    // MARK: - ImgTxtFld
    func onActionRight(_ imgTxtFld: ImageTextField) {
        becomeFirstResponder()
    }

}
