//
//  BaseCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

class BaseCoordinator: Coordinator {

    weak var presenter: UINavigationController!

    weak var parent: Coordinator?
    var children: [Coordinator] = []

    init(presenter: UINavigationController) {
        self.presenter = presenter
    }

    func start() {}

    func push(viewController: UIViewController) {
        presenter.pushViewController(viewController, animated: true)
    }

}
