//
//  FinishCoordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 20/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation
import LabCore

protocol FinishCoordinator: Coordinator {

    var didFinish: Observable<Self> { get set }

    func finish()

}

extension FinishCoordinator {

    func finish() {}

}
