//
//  Coordinator.swift
//  LabNutrition
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {

    var parent: Coordinator? { get set }
    var children: [Coordinator] { get set }

    func add(child coordinator: Coordinator)
    func remove(child coordinator: Coordinator)
    func start()
    func push(viewController: UIViewController)

}

extension Coordinator {

    func add(child coordinator: Coordinator) {
        coordinator.parent = self
        children.append(coordinator)
    }

    func remove(child coordinator: Coordinator) {
        children.removeAll { $0 === coordinator }
    }

    func push(viewController: UIViewController) {}

}
