//
//  BaseVM.swift
//  LabNutrition
//
//  Created by Victor Salazar on 29/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

protocol BaseVMProtocol {

    func loadView()

}
