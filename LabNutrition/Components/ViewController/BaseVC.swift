//
//  BaseVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 22/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol {

    func setupView()
    func setupBinding()

}

class BaseVC: UIViewController, BaseViewControllerProtocol {

    // MARK: - VC Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupBinding()
    }

    // MARK: - Protocol
    func setupView() {}
    func setupBinding() {}

}
