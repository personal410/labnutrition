//
//  AppDelegate.swift
//  LabNutrition
//
//  Created by Victor Salazar on 26/8/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn

@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate {

    static let testing = true
    private let googleKey = "170162067898-uq3bq9tu5g159nq7g516f8f85hues4lb.apps.googleusercontent.com"

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Coordinator
        let window = UIWindow()
        let coordinator = AppCoordinator(window: window)
        self.appCoordinator = coordinator
        self.window = window
        self.window?.makeKeyAndVisible()
        self.appCoordinator?.start()
        // FB
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        // Google
        GIDSignIn.sharedInstance()?.clientID = googleKey
        return true
    }

    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]) {
            return true
        } else {
            return GIDSignIn.sharedInstance().handle(url)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}

}
