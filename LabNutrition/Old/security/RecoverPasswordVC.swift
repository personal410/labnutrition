//
//  RecoverPasswordVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 27/8/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//
import UIKit

class RecoverPasswordVC: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var tfEmail: UITextField!

    // MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Action
    @IBAction func hideKeyboard() {
        tfEmail.resignFirstResponder()
    }

}
