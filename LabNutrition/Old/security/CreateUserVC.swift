//
//  CreateUserVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 4/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//
import UIKit
class CreateUserVC: UIViewController, ComboBoxDelegate {

    // MARK: - Outlets
    @IBOutlet weak var svContent: UIScrollView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfLastname: UITextField!
    @IBOutlet weak var tfDNI: UITextField!
    @IBOutlet weak var cbDocument: ComboBox!

    // MARK: - Props
    var idxComboBox = -1

    // MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        cbDocument.setValues(["Test1", "Test2"])
        if AppDelegate.testing {
            tfEmail.text = "vic@gmail.com"
            tfName.text = "Victor"
            tfLastname.text = "Salazar"
            tfDNI.text = "46124137"
            cbDocument.setSelectedIndex(0)
            idxComboBox = 0
        }
    }

    // MARK: - ComboBox
    func comboBox(_ comboBox: ComboBox, didSelectAt index: Int) {
        idxComboBox = index
    }

    // MARK: - Actions
    @IBAction func hideKeyboard() {
        tfEmail.resignFirstResponder()
        tfName.resignFirstResponder()
        tfLastname.resignFirstResponder()
        tfDNI.resignFirstResponder()
        cbDocument.resignFirstResponder()
    }

    @IBAction func actionContinue() {
        let email = tfEmail.text!
        if email.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su correo eletrónico", inViewCont: self)
            return
        }
        if !Toolbox.validate(email: email) {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese un correo eletrónico válido", inViewCont: self)
            return
        }
        let name = tfName.text!
        if name.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su nombre", inViewCont: self)
            return
        }
        let lastname = tfLastname.text!
        if lastname.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su apellido", inViewCont: self)
            return
        }
        let dni = tfDNI.text!
        if dni.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su DNI", inViewCont: self)
            return
        }
        if dni.count != 8 {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese un DNI válido", inViewCont: self)
            return
        }
        if idxComboBox == -1 {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Seleccione una opción", inViewCont: self)
            return
        }
        performSegue(withIdentifier: "showCreatePassword", sender: nil)
    }

    // MARK: - Keyboard
    @objc func showKeyboard(_ notification: Notification) {
        guard let info = notification.userInfo,
            let keyboardFrameValue = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight = keyboardFrameValue.cgRectValue.height
        svContent.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        svContent.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    }

    @objc func willHideKeyboard() {
        let newEdgeInset = UIEdgeInsets.zero
        svContent.contentInset = newEdgeInset
        svContent.scrollIndicatorInsets = newEdgeInset
    }

}
