//
//  TyCVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit

class TyCVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var ivChkTyC: UIImageView!
    @IBOutlet weak var ivChkTrans: UIImageView!

    // MARK: - Props
    var tycAccepted: Bool = false {
        didSet {
            ivChkTyC.backgroundColor = tycAccepted ? UIColor.red : UIColor.black
        }
    }

    var transAccepted: Bool = false {
        didSet {
            ivChkTrans.backgroundColor = transAccepted ? UIColor.red : UIColor.black
        }
    }

    // MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Actions
    @IBAction func changeCheck(_ btn: UIButton) {
        if btn.tag == 0 {
            tycAccepted = !tycAccepted
        } else {
            transAccepted = !transAccepted
        }
    }

    @IBAction func actionContinue() {
        if !tycAccepted {
            Toolbox.showAlertWithTitle(
                "Alerta",
                withMessage: "Falta aceptar los términos y condiciones",
                inViewCont: self
            )
            return
        }
        if !transAccepted {
            Toolbox.showAlertWithTitle(
                "Alerta",
                withMessage: "Falta aceptar el consentimiento para el tratamiento de datos",
                inViewCont: self
            )
            return
        }
        performSegue(withIdentifier: "showWelcome", sender: nil)
    }

}
