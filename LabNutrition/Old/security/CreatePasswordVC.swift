//
//  CreatePasswordVC.swift
//  LabNutrition
//
//  Created by Victor Salazar on 5/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit
import LabFoundation

class CreatePasswordVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var ivSwitch: UIImageView!

    // MARK: - Props
    var showPassword = true

    // MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDelegate.testing {
            tfPassword.text = "123456"
            tfConfirmPassword.text = "123456"
        }
    }

    // MARK: - Actions
    @IBAction func changeShowPassword() {
        showPassword = !showPassword
        ivSwitch.tintColor = showPassword ? .projectRed : .lightGray
        ivSwitch.transform = showPassword ? .identity : .init(rotationAngle: CGFloat(Double.pi))
        tfPassword.isSecureTextEntry = !showPassword
        tfConfirmPassword.isSecureTextEntry = !showPassword
    }

    @IBAction func hideKeyboard() {
        tfPassword.resignFirstResponder()
        tfConfirmPassword.resignFirstResponder()
    }

    @IBAction func actionContinue() {
        let password = tfPassword.text!
        if password.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su contraseña", inViewCont: self)
            return
        }
        let confirmPassword = tfConfirmPassword.text!
        if confirmPassword.isEmpty {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Ingrese su contraseña de confirmación", inViewCont: self)
            return
        }
        if password != confirmPassword {
            Toolbox.showAlertWithTitle("Alerta", withMessage: "Las contraseñas no coinciden", inViewCont: self)
            return
        }
        performSegue(withIdentifier: "showTyC", sender: nil)
    }

}
