//
//  BottomAlertHUDView.swift
//  LabNutrition
//
//  Created by Victor Salazar on 6/9/19.
//  Copyright © 2019 Victor Salazar. All rights reserved.
//

import UIKit
import LabFoundation

@objc protocol BottomAlertHUDViewDelegate {

    @objc optional func onClose()
    func onOk()

}

class BottomAlertHUDView: UIView {

    // MARK: - Props
    static var bottomAlertHUDView: BottomAlertHUDView?
    weak var delegate: BottomAlertHUDViewDelegate?

    // MARK: - Auxiliar
    static func showBottomAlert(
        with message: String,
        andBtn buttonTitle: String = "Continuar",
        and delegate: BottomAlertHUDViewDelegate? = nil) {
        let application = UIApplication.shared
        let window = application.windows.first!
        bottomAlertHUDView = BottomAlertHUDView(frame: window.frame)
        bottomAlertHUDView?.delegate = delegate
        bottomAlertHUDView?.backgroundColor = UIColor(sameValue: 0, with: 0.7)
        let maxSize = CGSize(width: window.frame.width - 80, height: CGFloat(Float.infinity))
        let strMessage = message as NSString
        let font = UIFont.systemFont(ofSize: 20)
        let textSize = strMessage.boundingRect(
            with: maxSize,
            options: .usesLineFragmentOrigin,
            attributes: [.font: font], context: nil)
        let intViewHeight = textSize.height + 160
        let internalView = UIView(
            frame: CGRect(
                x: 0,
                y: window.frame.height - intViewHeight,
                width: window.frame.width,
                height: intViewHeight
            )
        )
        internalView.backgroundColor = .white
        internalView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        internalView.cornerRadius = 10.0
        let btnClose = UIButton(frame: CGRect(x: 10, y: 10, width: 25, height: 25))
        btnClose.setImage(UIImage(named: "ic_x"), for: .normal)
        btnClose.addTarget(bottomAlertHUDView, action: #selector(actionClose), for: .touchUpInside)
        internalView.addSubview(btnClose)
        let lblMessage = UILabel(frame: CGRect(x: 40, y: 50, width: window.frame.width - 80, height: textSize.height))
        lblMessage.font = font
        lblMessage.text = message
        lblMessage.textAlignment = .center
        lblMessage.numberOfLines = 0
        internalView.addSubview(lblMessage)
        let btnOk = UIButton(frame: CGRect(x: 40, y: intViewHeight - 60, width: window.frame.width - 80, height: 40))
        btnOk.cornerRadius = 20
        btnOk.backgroundColor = .projectRed
        btnOk.setTitle(buttonTitle, for: .normal)
        btnOk.addTarget(bottomAlertHUDView, action: #selector(actionOk), for: .touchUpInside)
        internalView.addSubview(btnOk)
        bottomAlertHUDView?.addSubview(internalView)
        window.addSubview(bottomAlertHUDView!)
    }

    @objc func actionClose() {
        BottomAlertHUDView.bottomAlertHUDView?.removeFromSuperview()
        delegate?.onClose?()
    }

    @objc func actionOk() {
        BottomAlertHUDView.bottomAlertHUDView?.removeFromSuperview()
        delegate?.onOk()
    }

}
