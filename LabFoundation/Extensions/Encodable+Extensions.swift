//
//  Encodable+Extensions.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public extension Encodable {

    func jsonEncoded() throws -> Data {
        return try JSONEncoder().encode(self)
    }

}
