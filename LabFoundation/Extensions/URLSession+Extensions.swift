//
//  URLSession+Extensions.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

extension URLSession {

    func dataTask(
        for request: URLRequest,
        _ completion: @escaping (DataTaskResult) -> Void
    ) -> URLSessionDataTask {
        let dataTask = self.dataTask(with: request) { (data, response, error) in
            if let error = error {
                return completion(.failure(error: error))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(error: RequestError.noResponse))
            }
            guard (200...299).contains(response.statusCode) else {
                return completion(.failure(error: RequestError.statusCodeError(response.statusCode)))
            }
            completion(.success(data: data ?? Data()))
        }
        return dataTask
    }

}

extension URLSession {

    enum DataTaskResult {

        case success(data: Data)
        case failure(error: Error)

        var value: Data? {
            switch self {
            case .success(let data): return data
            default: return nil
            }
        }

        var error: Error? {
            switch self {
            case .success: return nil
            case .failure(let error): return error
            }
        }

        init(data: Data?, error: Error) {
            guard let data = data else {
                self = .failure(error: error)
                return
            }
            self = .success(data: data)
        }

    }
}
