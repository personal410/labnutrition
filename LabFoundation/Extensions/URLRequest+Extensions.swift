//
//  URLRequest+Extensions.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

extension URLRequest {

    init(_ request: BaseRequest) {
        let url = URL(request)
        self.init(url: url)
        httpMethod = request.method.rawValue
    }

    init<Request: EncondableRequest>(encodableRequest request: Request) {
        do {
            self.init(request)
            httpBody = try request.encode(request.body)
        } catch {
            fatalError("Encode failed")
        }
    }

}
