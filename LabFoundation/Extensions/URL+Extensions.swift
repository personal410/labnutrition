//
//  URL+Extensions.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

extension URL {

    init(_ request: BaseRequest) {
        let allowedChars = CharacterSet.urlPathAllowed.union(.urlQueryAllowed)
        guard let encondedPath = request.path.addingPercentEncoding(withAllowedCharacters: allowedChars) else {
            fatalError("Invalid path")
        }
        let raw = request.baseUrl.absoluteString + encondedPath
        guard let url = URL(string: raw) else {
            fatalError("Malformed url")
        }
        self = url
    }

}
