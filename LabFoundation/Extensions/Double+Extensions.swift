//
//  Double+Extensions.swift
//  LabFoundation
//
//  Created by Victor Salazar on 12/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public extension Double {

    var formattedString: String {
        String(format: "S/ %.2f", self)
    }

}
