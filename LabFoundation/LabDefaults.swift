//
//  LabDefaults.swift
//  LabFoundation
//
//  Created by Victor Salazar on 5/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public class LabDefaults {

    public static func save<Value>(value: Value?, for key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }

    public static func save<Value: Encodable>(value: Value?, for key: String) {
        guard let encoded = try? value.jsonEncoded() else { return }
        UserDefaults.standard.set(encoded, forKey: key)
    }

    public static func fetch<Value>(for key: String) -> Value? {
        return UserDefaults.standard.object(forKey: key) as? Value
    }

    public static func fetch<Value: Decodable>(for key: String) -> Value? {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else { return nil }
        return try? data.jsonDecoded()
    }

}
