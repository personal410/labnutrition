//
//  LabXMLParser.swift
//  LabFoundation
//
//  Created by Victor Salazar on 9/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class LabXMLParser: NSObject, XMLParserDelegate {

    private static var shared: LabXMLParser?

    private var completation: ((Data?) -> Void)!
    private var finalString: String = ""
    private var isList: Bool!

    static func parse(data: Data, isList: Bool = false, and completation: @escaping (Data?) -> Void) {
        shared = LabXMLParser()
        shared?.completation = completation
        shared?.isList = isList
        let xmlParser = XMLParser(data: data)
        xmlParser.delegate = shared
        xmlParser.parse()
    }

    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        completation(nil)
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let start = isList ? "[" : ""
        let end = isList ? "]" : ""
        finalString = "\(start)\(string)\(end)"
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        completation(finalString.data(using: .utf8))
        LabXMLParser.shared = nil
    }

}
