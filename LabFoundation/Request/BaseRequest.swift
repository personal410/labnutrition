//
//  BaseRequest.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public protocol BaseRequest {

    var baseUrl: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }

}

public extension BaseRequest {

    var method: HTTPMethod { .get }

}
