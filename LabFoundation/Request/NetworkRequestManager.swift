//
//  NetworkRequestManager.swift
//  LabFoundation
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public protocol NetworkRequestManager: AnyObject {

    var requestQueue: OperationQueue { get }
    var urlSession: URLSession { get }

}

public extension NetworkRequestManager {

    func operation<AnyRequest: NormalRequest>(
        for request: AnyRequest,
        _ completion: @escaping (Result<AnyRequest.Response, Error>) -> Void
    ) -> Operation {
        RequestOperation(delegate: self, request: request, completion: completion)
    }

    func operation<AnyEncodableRequest: EncondableRequest>(
        for request: AnyEncodableRequest,
        _ completion: @escaping (Result<AnyEncodableRequest.Response, Error>) -> Void
    ) -> Operation {
        EncodableRequestOperation(delegate: self, request: request, completion: completion)
    }

    func operation<AnyXmlRequest: XMLRequest>(
        for request: AnyXmlRequest,
        _ completion: @escaping (Result<AnyXmlRequest.Response, Error>) -> Void
    ) -> Operation {
        XMLOperation(delegate: self, request: request, completion: completion)
    }

    func load<AnyRequest: NormalRequest>(
        _ request: AnyRequest,
        _ completion: @escaping (Result<AnyRequest.Response, Error>) -> Void
    ) {
        requestQueue.addOperation(operation(for: request, completion))
    }

    func load<AnyEncodableRequest: EncondableRequest>(
        _ request: AnyEncodableRequest,
        _ completion: @escaping (Result<AnyEncodableRequest.Response, Error>) -> Void
    ) {
        requestQueue.addOperation(operation(for: request, completion))
    }

    func load<AnyXmlRequest: XMLRequest>(
        _ request: AnyXmlRequest,
        _ completion: @escaping (Result<AnyXmlRequest.Response, Error>) -> Void
    ) {
        requestQueue.addOperation(operation(for: request, completion))
    }

}
