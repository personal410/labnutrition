//
//  RequestError.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public enum RequestError: LocalizedError {

    case noResponse
    case statusCodeError(Int)
    case decodeError

}

public extension RequestError {

    var localizedDescription: String {
        switch self {
        case .noResponse:
            return "No Response"
        case .statusCodeError(let statusCode):
            return "Error statusCode: \(statusCode)"
        case .decodeError:
            return "Decode Error"
        }
    }

    var errorDescription: String? {
        switch self {
        case .noResponse:
            return "No Response"
        case .statusCodeError(let statusCode):
            return "Error statusCode: \(statusCode)"
        case .decodeError:
            return "Decode Error"
        }
    }

}
