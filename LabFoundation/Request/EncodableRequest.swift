//
//  EncodableRequest.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public protocol EncondableRequest: NormalRequest {

    associatedtype Body

    var body: Body { get }

    var encode: (Body) throws -> Data { get }

}

public extension EncondableRequest {

    var method: HTTPMethod { .post }

}

public extension EncondableRequest where Body: Encodable {

    var encode: (Body) throws -> Data {
        return { body in try body.jsonEncoded() }
    }

}
