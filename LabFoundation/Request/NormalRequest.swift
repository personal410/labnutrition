//
//  Request.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public protocol NormalRequest: BaseRequest {

    associatedtype Response

    var decode: (Data) throws -> Response { get }

}

public extension NormalRequest where Response: Decodable {

    var decode: (Data) throws -> Response {
        return { data in try data.jsonDecoded() }
    }

}
