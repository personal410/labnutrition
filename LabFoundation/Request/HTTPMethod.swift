//
//  HTTPMethod.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {

    case get = "GET"
    case post = "POST"

}
