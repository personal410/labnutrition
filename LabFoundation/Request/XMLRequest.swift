//
//  XMLRequest.swift
//  LabFoundation
//
//  Created by Victor Salazar on 9/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public protocol XMLRequest: NormalRequest {

    var isList: Bool { get }

}

public extension XMLRequest {

    var isList: Bool { false }

}
