//
//  EncodableRequestOperation.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class EncodableRequestOperation<Del: NetworkRequestManager, Req: EncondableRequest>: RequestOperation<Del, Req> {

    override func createURLRequest() -> URLRequest {
        URLRequest(encodableRequest: request)
    }

}
