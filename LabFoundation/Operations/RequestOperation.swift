//
//  RequestOperation.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class RequestOperation<Delegate: NetworkRequestManager, Request: NormalRequest>: AsyncOperation {

    weak var delegate: Delegate?
    let request: Request
    let completion: (Result<Request.Response, Error>) -> Void
    var dataTask: URLSessionDataTask?

    init(
        delegate: Delegate,
        request: Request,
        completion: @escaping (Result<Request.Response, Error>) -> Void) {
        self.delegate = delegate
        self.request = request
        self.completion = completion
    }

    override func main() {
        let urlRequest = createURLRequest()
        dataTask = delegate?.urlSession.dataTask(for: urlRequest, { [weak self] (dataTaskResult) in
            guard let self = self else { return }
            defer { self.state = .finished }
            guard !self.isCancelled && !self.isFinished else { return }
            switch dataTaskResult {
            case .failure(let error):
                self.completion(.failure(error))
            case .success(let data):
                guard let response = try? self.request.decode(data) else {
                    self.completion(.failure(RequestError.decodeError))
                    return
                }
                self.completion(.success(response))
            }
        })
        dataTask?.resume()
    }

    override func cancel() {
        super.cancel()
        dataTask?.cancel()
    }

    func createURLRequest() -> URLRequest {
        URLRequest(request)
    }

}
