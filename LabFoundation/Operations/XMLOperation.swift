//
//  XMLOperation.swift
//  LabFoundation
//
//  Created by Victor Salazar on 9/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

class XMLOperation<Delegate: NetworkRequestManager, Request: XMLRequest>: RequestOperation<Delegate, Request> {

    override func main() {
        let urlRequest = createURLRequest()
        dataTask = delegate?.urlSession.dataTask(for: urlRequest, { [weak self] (dataTaskResult) in
            guard let self = self else { return }
            defer { self.state = .finished }
            guard !self.isCancelled && !self.isFinished else { return }
            switch dataTaskResult {
            case .failure(let error):
                self.completion(.failure(error))
            case .success(let data):
                LabXMLParser.parse(data: data, isList: self.request.isList) { (finalData) in
                    guard let finalData = finalData else {
                        self.completion(.failure(RequestError.decodeError))
                        return
                    }
                    guard let response = try? self.request.decode(finalData) else {
                        self.completion(.failure(RequestError.decodeError))
                        return
                    }
                    self.completion(.success(response))
                }
            }
        })
        dataTask?.resume()
    }

}
