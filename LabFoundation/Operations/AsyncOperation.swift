//
//  AsyncOperation.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

open class AsyncOperation: Operation {

    public var state: State = .ready

    open override var isReady: Bool { super.isReady && state == .ready }
    open override var isExecuting: Bool { state == .executing }
    open override var isFinished: Bool { state == .finished }

    open override var isAsynchronous: Bool { true }

    open override func start() {
        guard !isCancelled && !isFinished else {
            state = .finished
            return
        }
        state = .executing
        main()
    }

    open override func cancel() {
        state = .finished
    }

}

public extension AsyncOperation {

    enum State: String {

        case ready
        case executing
        case finished

        fileprivate var keyPath: String {
            return "is\(rawValue.capitalized)"
        }

    }

}
