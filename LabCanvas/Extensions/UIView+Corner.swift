//
//  UIView+Corner.swift
//  LabCanvas
//
//  Created by Victor Salazar on 8/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public enum CornerType {

    case left
    case top
    case bottom

}

public extension UIView {

    var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }

    func set(cornerType: CornerType) {
        switch cornerType {
        case .left:
            layer.maskedCorners = [
                .layerMinXMinYCorner,
                .layerMinXMaxYCorner
            ]
        case .top:
            layer.maskedCorners = [
                .layerMinXMinYCorner,
                .layerMaxXMinYCorner
            ]
        case .bottom:
            layer.maskedCorners = [
                .layerMinXMaxYCorner,
                .layerMaxXMaxYCorner
            ]
        }
    }

}
