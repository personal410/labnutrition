//
//  UIViewController+Alert.swift
//  LabCanvas
//
//  Created by Victor Salazar on 6/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public struct AlertInformation {

    let title: String
    let mesage: String
    let okButtonTitle: String
    let cancelButtonTitle: String?
    let okHandler: (() -> Void)?

    public init(
        title: String,
        message: String,
        okButtonTitle: String = "Ok",
        cancelButtonTitle: String? = nil,
        okHandler: (() -> Void)? = nil
    ) {
        self.title = title
        self.mesage = message
        self.okButtonTitle = okButtonTitle
        self.cancelButtonTitle = cancelButtonTitle
        self.okHandler = okHandler
    }

}

public extension UIViewController {

    func showAlert(with alertInformation: AlertInformation) {
        let alertCont = UIAlertController(
            title: alertInformation.title,
            message: alertInformation.mesage,
            preferredStyle: .alert
        )
        alertCont.addAction(UIAlertAction(title: alertInformation.okButtonTitle, style: .default, handler: { (_) in
            alertInformation.okHandler?()
        }))
        if let cancelButtonTitle = alertInformation.cancelButtonTitle {
            alertCont.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil))
        }
        present(alertCont, animated: true, completion: nil)
    }

}
