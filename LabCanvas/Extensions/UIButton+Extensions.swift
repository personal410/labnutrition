//
//  UIButton+Extensions.swift
//  LabCanvas
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIButton {

    convenience init(image: UIImage) {
        self.init()
        setImage(image, for: .normal)
    }

    func setTitle(_ title: String, withFont font: UIFont, withColor color: UIColor) {
        let attributedString = NSAttributedString(
            string: title,
            attributes: [
                .font: font,
                .foregroundColor: color
            ]
        )
        setAttributedTitle(attributedString, for: .normal)
    }

}
