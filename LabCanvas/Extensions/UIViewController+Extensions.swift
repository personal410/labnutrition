//
//  UIViewController+Extensions.swift
//  LabCanvas
//
//  Created by Victor Salazar on 6/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIViewController {

    static func instantiate<T>() -> T {
        guard let storyboardName = String(describing: self).text(before: "VC") else {
            fatalError("Storyboard name error")
        }
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle(for: self))
        guard let viewController = storyboard.instantiateInitialViewController() as? T else {
            fatalError("Storyboard not found")
        }
        return viewController
    }

}

extension String {

    func text(before text: String) -> String? {
        guard let range = range(of: text) else { return nil }
        return String(self[startIndex ..< range.lowerBound])
    }

}
