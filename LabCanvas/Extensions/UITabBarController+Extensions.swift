//
//  UITabBarController+Extensions.swift
//  LabCanvas
//
//  Created by Victor Salazar on 6/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UITabBarController {

    func add(tab viewController: UIViewController) {
        if viewControllers == nil {
            viewControllers = [viewController]
        } else {
            viewControllers?.append(viewController)
        }
    }

}
