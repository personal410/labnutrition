//
//  UIImageView+Extensions.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.tintColorDidChange()
    }

}
