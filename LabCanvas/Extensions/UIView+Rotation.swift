//
//  UIView+Rotation.swift
//  LabCanvas
//
//  Created by Victor Salazar on 8/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIView {

    func set(rotationFactor: CGFloat) {
        transform = .init(rotationAngle: CGFloat.pi * rotationFactor)
    }

}
