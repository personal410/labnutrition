//
//  UILabel+Extensions.swift
//  LabCanvas
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UILabel {

    func setTextStrikethrough(_ text: String, in range: NSRange? = nil) {
        var finalRange: NSRange!
        if range != nil {
            finalRange = range
        } else {
            finalRange = NSRange(location: 0, length: text.count)
        }
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: finalRange)
        attributedText = attributedString
    }
}
