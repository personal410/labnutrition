//
//  UIView+Shadow.swift
//  LabCanvas
//
//  Created by Victor Salazar on 8/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public struct LabShadow {

    let color: UIColor
    let radius: CGFloat
    let opacity: Float
    let offset: CGSize

    public init(color: UIColor = .projectGray, radius: CGFloat = 2, opacity: Float = 0.75, offset: CGSize = .zero) {
        self.color = color
        self.radius = radius
        self.opacity = opacity
        self.offset = offset
    }

}

public extension UIView {

    func set(shadow: LabShadow) {
        layer.shadowColor = shadow.color.cgColor
        layer.shadowRadius = shadow.radius
        layer.shadowOpacity = shadow.opacity
        layer.shadowOffset = shadow.offset
    }

}
