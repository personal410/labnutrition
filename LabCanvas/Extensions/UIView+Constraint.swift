//
//  UIView+Constraint.swift
//  LabCanvas
//
//  Created by Victor Salazar on 11/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIView {

    func sameConstraint(attribute: NSLayoutConstraint.Attribute, to view: UIView, with constant: CGFloat = 0) {
        let layoutContraint = NSLayoutConstraint(
            item: self,
            attribute: attribute,
            relatedBy: .equal,
            toItem: view,
            attribute: attribute,
            multiplier: 1,
            constant: constant
        )
        addConstraint(layoutContraint)
    }

    func selfConstraint(attribute: NSLayoutConstraint.Attribute, with constant: CGFloat = 0) {
        let layoutContraint = NSLayoutConstraint(
            item: self,
            attribute: attribute,
            relatedBy: .equal,
            toItem: nil,
            attribute: attribute,
            multiplier: 1,
            constant: constant
        )
        addConstraint(layoutContraint)
    }

    func aspectRatio(with multiplier: CGFloat = 1) {
        let layoutContraint = NSLayoutConstraint(
            item: self,
            attribute: .width,
            relatedBy: .equal,
            toItem: self,
            attribute: .height,
            multiplier: multiplier,
            constant: 0
        )
        addConstraint(layoutContraint)
    }

    func setupInsideView(_ view: UIView, with space: CGFloat = 0) {
        sameConstraint(attribute: .left, to: view, with: -space)
        sameConstraint(attribute: .right, to: view, with: space)
        sameConstraint(attribute: .top, to: view, with: -space)
        sameConstraint(attribute: .bottom, to: view, with: space)
    }

}
