//
//  UIView+Border.swift
//  LabCanvas
//
//  Created by Victor Salazar on 8/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public struct LabBorder {

    let color: UIColor
    let width: CGFloat

    public init(color: UIColor, width: CGFloat = 1) {
        self.color = color
        self.width = width
    }

}

public extension UIView {

    func set(border: LabBorder) {
        layer.borderColor = border.color.cgColor
        layer.borderWidth = border.width
    }

}
