//
//  LabView.swift
//  LabCanvas
//
//  Created by Victor Salazar on 13/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public class LabView: UIView {

    // MARK: - Props
    public var addGradient = false
    public var startColor = UIColor.clear
    public var endColor = UIColor.black
    private var wasGradientAdded = false

    // MARK: - Draw
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        if addGradient && !wasGradientAdded {
            wasGradientAdded = true
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            gradientLayer.frame = rect
            layer.addSublayer(gradientLayer)
        }
    }

}
