//
//  LabColor.swift
//  LabCanvas
//
//  Created by Victor Salazar on 7/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIColor {

    convenience init(nRed: Int, nGreen: Int, nBlue: Int, with alpha: CGFloat = 1) {
        let red = CGFloat(nRed) / 255.0
        let green = CGFloat(nGreen) / 255.0
        let blue = CGFloat(nBlue) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    convenience init(sameValue rgb: Int, with alpha: CGFloat = 1) {
        self.init(nRed: rgb, nGreen: rgb, nBlue: rgb, with: alpha)
    }

    static let projectRed = UIColor(nRed: 255, nGreen: 51, nBlue: 58)
    static let projectPink = UIColor(nRed: 255, nGreen: 219, nBlue: 221)
    static let projectBlue = UIColor(nRed: 24, nGreen: 119, nBlue: 236)
    static let projectYellow = UIColor(nRed: 255, nGreen: 165, nBlue: 48)
    static let projectGray = UIColor(sameValue: 154)
    static let projectWhite50 = UIColor.white.withAlphaComponent(0.5)
    static let projectBlack3 = UIColor(sameValue: 33)

}
