//
//  LabFont.swift
//  LabCanvas
//
//  Created by Victor Salazar on 6/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public enum LabFontType: String {

    case light
    case regular
    case bold

}

public extension UIFont {

    static func labFont(with size: CGFloat = 18, and type: LabFontType = .regular) -> UIFont {
        guard let font = UIFont(name: "Oswald-\(type.rawValue.capitalized)", size: size) else {
            fatalError("Font not found")
        }
        return font
    }

}
