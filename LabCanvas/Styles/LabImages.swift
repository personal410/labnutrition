//
//  UIImage+Assets.swift
//  LabCanvas
//
//  Created by Victor Salazar on 7/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public extension UIImage {

    convenience init(canvas name: String) {
        self.init(named: name, in: Bundle(for: LabCanvas.self), compatibleWith: nil)!
    }

    // MARK: - TabBar
    static let icHome = UIImage(canvas: "ic_home")
    static let icBenefits = UIImage(canvas: "ic_benefits")
    static let icStore = UIImage(canvas: "ic_store")
    static let icShoppingCart = UIImage(canvas: "ic_shopping_cart")
    static let icMore = UIImage(canvas: "ic_more")

    // MARK: - Icons
    static let icMedal = UIImage(canvas: "ic_medal")
    static let icBell = UIImage(canvas: "ic_bell")
    static let icBulb = UIImage(canvas: "ic_bulb")
    static let icBag = UIImage(canvas: "ic_bag")
    static let icLabcard = UIImage(canvas: "ic_labcard")
    static let icUser = UIImage(canvas: "ic_user")
    static let icLocation = UIImage(canvas: "ic_location")
    static let icKey = UIImage(canvas: "ic_key")
    static let icGetOut = UIImage(canvas: "ic_get_out")
    static let icFB = UIImage(canvas: "ic_fb")
    static let icGoogle = UIImage(canvas: "ic_google")
    static let icPromotions = UIImage(canvas: "ic_promotions")
    static let icSearch = UIImage(canvas: "ic_search")
    static let icArrowDown = UIImage(canvas: "ic_arrow_down")
    static let icFilter = UIImage(canvas: "ic_filter")
    static let icArm = UIImage(canvas: "ic_arm")
    static let icLighting = UIImage(canvas: "ic_lighting")
    static let icBack = UIImage(canvas: "ic_back")
    static let icCalendar = UIImage(canvas: "ic_calendar")
    static let icShare = UIImage(canvas: "ic_share")
    static let icHeart = UIImage(canvas: "ic_heart")
    static let icWaist = UIImage(canvas: "ic_waist")
    static let icChronometer = UIImage(canvas: "ic_chronometer")
    static let icChest = UIImage(canvas: "ic_chest")
    static let icGymWeight = UIImage(canvas: "ic_gym_weight")
    static let icClose = UIImage(canvas: "ic_close")

    // MARK: - Images
    static let imgSwitch = UIImage(canvas: "img_switch")
    static let imgLab = UIImage(canvas: "img_lab")
    static let imgHome = UIImage(canvas: "img_home")
    static let imgVerticalPromotions = UIImage(canvas: "img_vertical_promotions")

    // MARK: - Background
    static let bgGirl = UIImage(canvas: "bg_girl")
    static let bgBoy = UIImage(canvas: "bg_boy")

    // MARK: - Components
    static let icChecked = UIImage(canvas: "ic_checked")
    static let icUnchecked = UIImage(canvas: "ic_unchecked")
    static let icCircleChecked = UIImage(canvas: "ic_circle_checked")
    static let icCircleUnchecked = UIImage(canvas: "ic_circle_unchecked")
    static let icSwitch = UIImage(canvas: "ic_switch")

}
