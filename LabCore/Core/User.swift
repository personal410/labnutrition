//
//  User.swift
//  LabCore
//
//  Created by Victor Salazar on 5/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct User: Codable {

    public let userId: String
    public let logInMethod: LogInMethod
    public let email: String
    public let names: String
    public let lastName: String
    var profile: Profile

    public var fullName: String {
        "\(names) \(lastName)"
    }

    public init(
        userId: String,
        logInMethod: LogInMethod,
        email: String,
        names: String,
        lastName: String,
        profile: Profile
    ) {
        self.userId = userId
        self.logInMethod = logInMethod
        self.email = email
        self.names = names
        self.lastName = lastName
        self.profile = profile
    }

    enum CodingKeys: String, CodingKey {

        case userId
        case logInMethod
        case email
        case names
        case lastName
        case profile

    }

}
