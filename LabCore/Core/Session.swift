//
//  Session.swift
//  LabCore
//
//  Created by Victor Salazar on 5/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

public class Session {

    public static let shared = Session()

    public var user: User?
    public var profile: Profile? { user?.profile }
    public var isUser: Bool { user != nil }
    public var isNewUser = false

    init() {
        user = LabDefaults.fetch(for: LabDefaults.Key.user)
    }

    public func saveUser(
        userId: String,
        logInMethod: LogInMethod,
        email: String,
        names: String,
        lastname: String
    ) {
        let newUser = User(
            userId: userId,
            logInMethod: logInMethod,
            email: email,
            names: names,
            lastName: lastname,
            profile: .empty
        )
        saveUser(newUser)
    }

    public func saveUser(_ newUser: User?) {
        LabDefaults.save(value: newUser, for: LabDefaults.Key.user)
        isNewUser = true
        user = newUser
    }

    public func updatePersonalInfo(age: Int, gender: String, height: Double, weight: Double) {
        user?.profile.age = age
        user?.profile.gender = gender
        user?.profile.height = height
        user?.profile.weight = weight
        LabDefaults.save(value: user, for: LabDefaults.Key.user)
    }

    public func updatePhysicalActivities(_ arrPhysicalActivity: [PhysicalActivity]) {
        user?.profile.arrPhysicalActivities = arrPhysicalActivity
        LabDefaults.save(value: user, for: LabDefaults.Key.user)
    }

    public func updateTrainingLevel(_ trainingLevel: TrainingLevel) {
        user?.profile.trainingLevel = trainingLevel
        LabDefaults.save(value: user, for: LabDefaults.Key.user)
    }

    public func updateObjectiveIds(_ arrObjectiveIds: [Int]) {
        user?.profile.arrObjectiveIds = arrObjectiveIds
        LabDefaults.save(value: user, for: LabDefaults.Key.user)
    }

    public func closeSession() {
        saveUser(nil)
    }

}
