//
//  PhysicalActivity.swift
//  LabCore
//
//  Created by Victor Salazar on 27/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct PhysicalActivity: Codable {

    // MARK: - Default
    static var allDefaults: [PhysicalActivity] {
        [
            .init(name: "Voley"),
            .init(name: "Fulbol"),
            .init(name: "Crossfit"),
            .init(name: "Fitness"),
            .init(name: "Deportes extremos"),
            .init(name: "Artes marciales"),
            .init(name: "Otras disciplinas")
        ]
    }

    // MARK: - Props
    public var name: String
    public var isSelected: Bool = false

    public init(name: String, isSelected: Bool = false) {
        self.name = name
        self.isSelected = isSelected
    }

    enum CodingKeys: String, CodingKey {

        case name
        case isSelected

    }

}
