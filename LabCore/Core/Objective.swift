//
//  Objective.swift
//  LabCore
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Objective {

    public static var all: [Objective] {
        [
            Objective(objectiveId: 1, description: "construye músculo"),
            Objective(objectiveId: 2, description: "aumenta tu energía"),
            Objective(objectiveId: 3, description: "salud y bienestar"),
            Objective(objectiveId: 4, description: "pierde peso y tonifa"),
            Objective(objectiveId: 5, description: "mejora tu rendimiendo"),
            Objective(objectiveId: 6, description: "ganar peso y volumen"),
            Objective(objectiveId: 7, description: "definición muscular")
        ]
    }

    public let objectiveId: Int
    public let description: String

}
