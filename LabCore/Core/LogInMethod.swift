//
//  LogInMethod.swift
//  LabNutrition
//
//  Created by Victor Salazar on 2/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import UIKit

public enum LogInMethod: Int, Codable {

    case facebook
    case gmail

}
