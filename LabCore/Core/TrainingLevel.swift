//
//  TrainingLevel.swift
//  LabCore
//
//  Created by Victor Salazar on 6/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public enum TrainingLevel: Int, Codable {

    case beginner
    case intermediate
    case advanced

    public static var all: [TrainingLevel] {
        return [.beginner, .intermediate, .advanced]
    }

}
