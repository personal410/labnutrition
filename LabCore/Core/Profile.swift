//
//  Profile.swift
//  LabCore
//
//  Created by Victor Salazar on 28/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Profile: Codable {

    // MARK: - Default
    static var empty: Profile {
        .init(
            age: -1,
            gender: "",
            height: -1,
            weight: -1,
            arrPhysicalActivities: PhysicalActivity.allDefaults,
            trainingLevel: .beginner,
            arrObjectiveIds: []
        )
    }

    // MARK: - Props
    public var age: Int
    public var gender: String
    public var height: Double
    public var weight: Double
    public var arrPhysicalActivities: [PhysicalActivity]
    public var trainingLevel: TrainingLevel
    public var arrObjectiveIds: [Int]

    enum CodingKeys: String, CodingKey {

        case age
        case gender
        case height
        case weight
        case arrPhysicalActivities
        case trainingLevel
        case arrObjectiveIds

    }
}
