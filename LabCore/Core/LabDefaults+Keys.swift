//
//  LabDefaults+Keys.swift
//  LabCore
//
//  Created by Victor Salazar on 5/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

extension LabDefaults {

    class Key {

        static let user = Bundle.main.bundleIdentifier! + ".user"

    }

}
