//
//  Observable.swift
//  Core
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Observable<Input> {

    private var callback: ((Input) -> Void)?

    public init() {}

    public mutating func bind<Observer: AnyObject>(
        to observer: Observer,
        with callback: @escaping ((Observer, Input) -> Void)) {
        self.callback = { [weak observer] input in
            guard let observer = observer else { return }
            callback(observer, input)
        }
    }

    public func notify(with input: Input) {
        callback?(input)
    }

}
