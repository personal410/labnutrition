//
//  BaseRequest+Extensions.swift
//  LabCore
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

public extension BaseRequest {

    var baseUrl: URL { URL(string: CoreRequestManager.shared.baseUrlString)! }

}
