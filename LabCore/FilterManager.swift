//
//  FilterManager.swift
//  LabCore
//
//  Created by Victor Salazar on 1/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public class FilterManager {

    public static let shared = FilterManager()

    public var arrObjectiveSelected: [Int] = []
    public var arrCategorySelected: [Int] = []
    public var maximumPrice: Int = 250
    public var arrBrandSelected: [Int] = []

}
