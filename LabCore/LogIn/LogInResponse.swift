//
//  LogInResponse.swift
//  LabNutrition
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct LogInResponse: Decodable {

    public var name: String
    public var lastName: String

    enum CodingKeys: String, CodingKey {

        case name
        case lastName = "last_name"

    }

}
