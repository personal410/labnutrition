//
//  LogInRequest.swift
//  LabNutrition
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

public struct LogInRequest: NormalRequest {

    public typealias Response = LogInResponse

    public let path: String = "/user"

    public init() {}

}
