//
//  Benefit.swift
//  LabCore
//
//  Created by Victor Salazar on 21/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Benefit {

    public let title: String
    public let description: String

    public static let benefit1 = Benefit(title: "BODYTECH", description: "1 Mes GRATIS en el plan semestral.")
    public static let benefit2 = Benefit(title: "CINEMARK", description: "2 Meses GRATIS en el plan anual.")

}
