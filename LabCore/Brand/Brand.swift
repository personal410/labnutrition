//
//  Brand.swift
//  LabCore
//
//  Created by Victor Salazar on 6/8/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Brand {

    public static var all: [Brand] {
        [
            Brand(brandId: 1, description: "MET-Rx"),
            Brand(brandId: 2, description: "Cellucorp"),
            Brand(brandId: 3, description: "Muscletech"),
            Brand(brandId: 4, description: "PowerCrunch"),
            Brand(brandId: 5, description: "SmartShake")
        ]
    }

    public let brandId: Int
    public let description: String

}
