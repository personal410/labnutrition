//
//  BlogNews.swift
//  LabCore
//
//  Created by Victor Salazar on 13/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct BlogNews {

    public let title: String

    public static let blogNewsTest1 = BlogNews(title: "Desayunos para este verano 2020")
    public static let blogNewsTest2 = BlogNews(title: "Almuerzos para este otoño 2019")

}
