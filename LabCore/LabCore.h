//
//  LabCore.h
//  LabCore
//
//  Created by Victor Salazar on 19/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for LabCore.
FOUNDATION_EXPORT double LabCoreVersionNumber;

//! Project version string for LabCore.
FOUNDATION_EXPORT const unsigned char LabCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LabCore/PublicHeader.h>


