//
//  Product.swift
//  LabCore
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Product: Decodable {

    public let codigo: String
    public let marca: String
    public let descripcion: String
    public let unidadMedida: String
    public let precioUnitario: Double
    public let fechaCreacion: String

    public var puntos: String {
        let puntos = max(precioUnitario.rounded(.down), 1)
        return String(format: "%.0f pt%@", puntos, puntos == 1 ? "" : "s")
    }

    public var precioPromocion: Double {
        precioUnitario * 0.9
    }

    enum CodingKeys: String, CodingKey {

        case codigo = "codf"
        case marca = "marc"
        case descripcion = "descr"
        case unidadMedida = "umed"
        case precioUnitario = "preu"
        case fechaCreacion = "fecreg"

    }

    public static let productTest = Product(
        codigo: "001",
        marca: "Lab",
        descripcion: "Galletas",
        unidadMedida: "UND",
        precioUnitario: 5,
        fechaCreacion: "20/02/20"
    )

    public static let productTest2 = Product(
        codigo: "002",
        marca: "Lab 2",
        descripcion: "Galletas 2",
        unidadMedida: "UND",
        precioUnitario: 6.5,
        fechaCreacion: "20/02/20"
    )

}
