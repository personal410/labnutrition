//
//  ProductListRequest.swift
//  LabCore
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

public struct ProductListRequest: XMLRequest {

    public typealias Response = [Product]

    public var path: String

    public var isList: Bool = true

    public init(numberLastDays: Int = 100) {
        path = "/getnewitems?ultdias=\(numberLastDays)&tipo=JSON"
    }

}
