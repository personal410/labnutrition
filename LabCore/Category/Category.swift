//
//  Category.swift
//  LabCore
//
//  Created by Victor Salazar on 31/7/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct LabCategory {

    public static var all: [LabCategory] {
        [
            LabCategory(categoryId: 1, description: "Quemadores de grasa"),
            LabCategory(categoryId: 2, description: "Ganadores de masa"),
            LabCategory(categoryId: 3, description: "Multivitamínicos"),
            LabCategory(categoryId: 4, description: "Pre entreno"),
            LabCategory(categoryId: 5, description: "Proteínas"),
            LabCategory(categoryId: 6, description: "Snacks saludables"),
            LabCategory(categoryId: 7, description: "Aminoácidos y Bcaa's"),
            LabCategory(categoryId: 8, description: "Accesorios"),
            LabCategory(categoryId: 9, description: "Post entreno y recuperadores")
        ]
    }

    public let categoryId: Int
    public let description: String

}
