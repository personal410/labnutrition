//
//  CoreRequestManager.swift
//  Core
//
//  Created by Victor Salazar on 18/5/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import LabFoundation

public class CoreRequestManager: NetworkRequestManager {

    public static let shared = CoreRequestManager()

    public let requestQueue: OperationQueue
    public let urlSession: URLSession

    public let baseUrlString = "http://181.65.167.118:8087/billdata/test/billdata.asmx"

    // Local: http://localhost:3000
    // Lab: http://181.65.167.118:8087/billdata/test/billdata.asmx

    init() {
        requestQueue = OperationQueue()
        requestQueue.qualityOfService = .background
        requestQueue.maxConcurrentOperationCount = 3
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.timeoutIntervalForRequest = 60
        urlSessionConfiguration.timeoutIntervalForResource = 60
        urlSessionConfiguration.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlSession = URLSession(configuration: urlSessionConfiguration)
    }

}
