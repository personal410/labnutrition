//
//  Promotion.swift
//  LabCore
//
//  Created by Victor Salazar on 10/6/20.
//  Copyright © 2020 Victor Salazar. All rights reserved.
//

import Foundation

public struct Promotion {

    public let productName: String
    public let brand: String
    public let promotionPrice: String
    public let previousPrice: String

    public static let promotionTest = Promotion(
        productName: "ULTRAMYOSYN WHY",
        brand: "MET RX",
        promotionPrice: "S/ 15.00",
        previousPrice: "S/ 20.00"
    )

}
